package org.jeecg.common.aspect.annotation;

/**
 * 用于平安的数据更新
 */
public @interface PingAnDataUpdate {

    String value() default "";

}
