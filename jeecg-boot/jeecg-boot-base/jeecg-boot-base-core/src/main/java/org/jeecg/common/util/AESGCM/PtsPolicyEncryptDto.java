package org.jeecg.common.util.AESGCM;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName PtsPolicyEncryptDto
 * @Description TODO
 * @Author xiaowen
 * @Date 2022/9/7 16:58
 * @Version 1.0
 **/
@Data
public
class PtsPolicyEncryptDto implements Serializable {
    /**
     * 加密内容
     */
    private String content;

    /**
     * 签名
     */
    private String sign;
}
