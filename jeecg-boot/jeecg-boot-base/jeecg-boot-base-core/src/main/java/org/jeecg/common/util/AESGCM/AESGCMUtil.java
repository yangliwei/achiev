package org.jeecg.common.util.AESGCM;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.online.config.exception.BusinessException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * 此类用AES加解密，用RSA加验签
 *
 * @author ex-quchao005
 * @date 2021-07-08 14:10
 */
@Slf4j
public class AESGCMUtil {
    /**
     * 签名算法
     */
    private static final String SIGNATURE_INSTANCE = "SHA1WithRSA";
    /**
     * AES标准算法 SHA1PRNG
     */
    private static final String ALGORITHM_SHA1PRNG = "SHA1PRNG";

    /**
     * 加签私钥
     */
    public static String SHA_PRIVATE_KEY = "MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQCvRT0uR5Ty5kFqs7BNJl0Ahiu34NkJGfBgLHXmDYYBUf1A+2MJU+Oj3BDD4BI4lFCEH/jWM6nD7lN137dBPba+QWQgSseIbPxYe9rptgX7W4sGCQT7/YYt2g9fVJKAFaA0u3Tw74ae0Xi0IChhio4lLaNhfhKBT5EnVtPn1R7NParsZV+TH5V2cLMY5AOfYPk7p8+X/1bVP4t3wuMslfBKYH6IOFhCY/V3sMVxFL/Z3fd65P/q0SPHFs7faEhyyjGpNF60H8an2aXLtAYcPLq/iEpSulbvDTtJAlhCelEReLGkHKl5UwJ81RiHRKv0KOSPp2e8cI1mXYHQcXEIQuyeARtQMHIWZy6mfQlNvmSLovHmroROg5CV6YLTIxlHH01+YnPsA0PFzmQIT4tB5TXVJK8Rfx6J36TI+rO4Fm+8may01HZtYa8w0xjqpm6e21fPqVaLcNxNYxqiCUlUk/kstjj+bgFUJWP5e40QjzpXnBhy6mt6agrrbKLymk/OWspu1UMBsp5dGoCM4aXt2o3CbXJhOWzwd52MiCqHlCxFlrQsE4+Xjbf1SGXdNpl1BV+EA8BjNTKGsBOfkWhIg0UgjEKizBVvcwbQ4Buyzt13SsG6QsblonKOiWnIlLlXlTu2GbE5pF4KEDPN6nw222IFW0tYU5oCT6pMmzQ6nZQCiQIDAQABAoICACk9JVQKS6WLP8EXXlgIusEw0ICHSwm1LLW0QFlMvUb6P5hWjcrYDiuzz84GnsLpQriTvTzJExyiqsiFRtz25FSHlkNy1NforMc0/SEYTvgBZgiZfq67zCJluFA52e6BKBzALpeyA/7CUqAvINEyhJeC0XpS64+5ZPHYoCtgHcs0w06z23UGbWhcxAcLPz701i+PIG0nCwtJ1Ertc/49mTgm1B7BAyM72bm7wfkFZM6iR2SHFR/u7XgeibZPFxzLLjwdslq3T0qXD0eKqBVkxJvkOXiVl3bPqi9XeeTKWuC9gS6X/9goC+HLUeYh5kOgi4axYNsZsGtxfOXKi0niVUHEOlIpVFyAV8vaKfdYeMAC4iLaX9nHA8UKeOsbXW50tLTgeR//JktozQMiyhEj6coWkmqSydbdjiHOyQKHHNG7hV2mT7k29e4mYtxMUAb+Gqeiush5YS2WgOMfWjZsJNXwUIKTl0A0DPJSxUmZs75fyrMQRRputzluQwiCgxHx6ZsWhvyg/rRRO2MqwW3+ye2N2xj/v3vTP1mEpq6ESzojcO7buT2oGS8acxkOP6ZfAhMorjkcs9oosK+KQnaV1Mwk1zhHk7Gw/K100lRGgtiEZKYeurkTQJpUQmpWmR2TrgCgIZ722dXBqI4iMz741FuhwWSOZS8sRG0i5hvmxkjBAoIBAQDex1RKbLp3aD2Dx46HwRXZ9wws94vLPc9gphTuBAiprNxg+B5TGdDDIn03BFSwkFvXD62DCVLRS4bNG0FyN4F2TlYA/rEZ8kS91+KDWXWSv3MAwDE2bzwRPIe0ZfreJWyU58pzZ9EyEdTACxEn3YLa04wGvNEDG7qcijcAPp1/3LvgmEQjX03fQhEGWFcypMpo7AOgPDyUibttB19ZFBoNeMzycWnlSDZTJuUSAci0H+8RrRSpPdlt66sqLJemeAnY9uLmSw64sT60CW3ci3P16lPhqq5dAxTrXyI6vDNlkg49OoyIlZgqhHpDkWUzmuZi0cGuyHkszZid5BnVVydFAoIBAQDJaEPSyY/r/CFapWYv7gcGGvdAfgQEZn9gDkGwZHAHvMNjma3O31bge5WXaU9jnTTD0bVQR8o5fzAIoFgXlHToJfihcLRETIrsh5QBJKVMBV+wtxq9fI7EtghDzJwUEkWF8zBTKBdh3rAxQRySxUnWs2wvlT/RSpCMJ37hQ0Eb0NyXQ46+JBOMiA5yxKTuCZGwC0P7/wmBzfGUQsfkEmstI/VD6eSWdZpfqQeo7jEFeGP1LHIGqQ7NO2iAK0Y1Ig6jTWQki7wXy94//0I1vQLhEv3uWC9WAmh4T1aUXrDS1RS70gBPGj+ipD1+HsvgbzCqj+3oD2a2jvu4dhEHa9B1AoIBAHd3u3sh7aklLH/j30prwFUgB9aBdeUrI2+4ANdXMHIVciVKSbDU0kAi2bVMTEJXV/jvvmPb9x6n+wU5DZK7s2CHR9XGv6CjbDvGbwFzUvq8UmbfqkR/yF2R8VBLtAm3VhBQcSAMV4IbT9Ug2jVfs9G3yoq+bPPWxe82q6Ik8f9klxgtb7TsYe1NF6Yob7KH7sfvcU5Xn0Ekz277miVCA2qgEoAdU+WSIRfcbtGGRWRbD7CR+dd8hIJO9H5sZXLvQpCobSuEqsNewbp4qAKygiApZoxJJLJaebhzTv7ieEmK1rn9EOoaFv0OCCb97KCM++mYhN4/QcRGCQb7EON4lmUCggEAQNA+0zxuBkES8rq7Ub1FoV9YJfbnkArz/pNX7E3GB5dlQU4ZMrOIn6DlmN4Y4DOc4U9BC96uYyZc7Mm8z4nEvVhK1JkSSdBf/JH8vQlv8DDihFnITyYgQ4lYy4iyyXq/yMVjzb7fF220QYV0jn7Srjb7H6zl5pIaRiAHorIj57Gk60CgVOwDJnw6OhCyCrAlpANcE2IVSyJdFZsP/IM43kPuD7bbCtg5n4MjpedQ+zOs5x8waphI3R67GFPUuhCFqWOkUGZvVzESCoI4alay8H9OHD9grEsGF4FgmUqHmLFdhLulwc7un4Y/C7EZZtYhUSIUK1OZeDI3KztDa6H0aQKCAQB4Y1KADgkvdaAxTPZeuwhYzGLJ4Oqz+nBWuPMs4WjT9uvXYBf+0wLazTdzAs7B0Jhlb/bbDFMyBHtwh6bDgo2a06AyCS4srcvfC0vTmGSJ3xYWw/jrYDPzDxxckVL7DRs2GD4uJ5agqwS200OBj3Jm19+x1Egg27UFv5CTbvKQk4ccHMSHMVJRxeKJPP5+b/eXd2XnpYKbtICS4IZXdnU0ylNaqhbbMZV2uRRYhFmp56G/6U13ZZwDfx2dXzCqV4doHsYIDq1fwlLclmdVJSnV28L2L8isoP3c/Tt4B2VJLL5FVbXOyX3+YsErAKuRXH8UKNsvSHxuiV5FVzTi8y9a";
    /**
     * 验签公钥
     */
    public static String SHA_PUBLIC_KEY = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAr0U9LkeU8uZBarOwTSZdAIYrt+DZCRnwYCx15g2GAVH9QPtjCVPjo9wQw+ASOJRQhB/41jOpw+5Tdd+3QT22vkFkIErHiGz8WHva6bYF+1uLBgkE+/2GLdoPX1SSgBWgNLt08O+GntF4tCAoYYqOJS2jYX4SgU+RJ1bT59UezT2q7GVfkx+VdnCzGOQDn2D5O6fPl/9W1T+Ld8LjLJXwSmB+iDhYQmP1d7DFcRS/2d33euT/6tEjxxbO32hIcsoxqTRetB/Gp9mly7QGHDy6v4hKUrpW7w07SQJYQnpREXixpBypeVMCfNUYh0Sr9Cjkj6dnvHCNZl2B0HFxCELsngEbUDByFmcupn0JTb5ki6Lx5q6EToOQlemC0yMZRx9NfmJz7ANDxc5kCE+LQeU11SSvEX8eid+kyPqzuBZvvJmstNR2bWGvMNMY6qZunttXz6lWi3DcTWMaoglJVJP5LLY4/m4BVCVj+XuNEI86V5wYcupremoK62yi8ppPzlrKbtVDAbKeXRqAjOGl7dqNwm1yYTls8HedjIgqh5QsRZa0LBOPl4239Uhl3TaZdQVfhAPAYzUyhrATn5FoSINFIIxCoswVb3MG0OAbss7dd0rBukLG5aJyjolpyJS5V5U7thmxOaReChAzzep8NttiBVtLWFOaAk+qTJs0Op2UAokCAwEAAQ==";
    /**
     * 秘钥
     */
    public static String AES_KEY = "UhkskUmxEh+U1zsoBI/21A==";
    /**
     * 加密算法
     */
    private static final String KEY_ALGORITHM = "AES";
    /**
     * 非对称加密密钥算法
     */
    private static final String KEY_ALGORITHM_RSA = "RSA";
    /**
     * 算法/加密模式/填充方式
     */
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/GCM/NoPadding";
    /**
     * 字符集
     */
    private static final String CHARSET = "UTF-8";
    /**
     * 秘钥长度 128位
     */
    private static final int KEY_SIZE = 128;
    /**
     * 长度算法因子-12
     */
    private static final int LEN_TWELVE= 12;
    /**
     * 长度算法因子-16
     */
    private static final int LEN_SIXTEEN= 16;

    /**
     * 加密-GCM
     *
     * @param content     明文
     * @param encryptPass 秘钥
     * @return 密文
     */
    public static String encrypt(String content, String encryptPass) {
        try {
            //随机生成偏移量
            byte[] iv = new byte[LEN_TWELVE];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(iv);
            byte[] contentBytes = content.getBytes(CHARSET);
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            GCMParameterSpec params = new GCMParameterSpec(KEY_SIZE, iv);
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(encryptPass), params);
            byte[] encryptData = cipher.doFinal(contentBytes);
            if (encryptData.length != contentBytes.length + LEN_SIXTEEN) {
                throw new Exception();
            }
            return makEncryptByIv(iv, encryptData, contentBytes.length);
        } catch (Exception e) {
           log.error("encrypt error... {}", e);
        }
        return null;
    }

    /**
     * 将偏移量拼接进密文
     *
     * @param iv      偏移量
     * @param encrypt 初始密文
     * @param cntLen  密钥
     * @return 密文
     */
    public static String makEncryptByIv(byte[] iv, byte[] encrypt, int cntLen) {
        byte[] message = new byte[LEN_TWELVE + cntLen + LEN_SIXTEEN];
        System.arraycopy(iv, 0, message, 0, LEN_TWELVE);
        System.arraycopy(encrypt, 0, message, LEN_TWELVE, encrypt.length);
        return Base64.getEncoder().encodeToString(message);
    }

    /**
     * 得到加密,加签后参数
     */
    public static String getEncryptStr(String jsonStr) throws Exception {
        //加密
        String content = AESGCMUtil.encrypt(jsonStr, AES_KEY);
        //加签
        String sign = AESGCMUtil.sign(content, SHA_PRIVATE_KEY);
        Map encryptMap = new HashMap();
        encryptMap.put("content", content);
        encryptMap.put("sign", sign);
        String encryptStr = JSON.toJSONString(encryptMap);

        return encryptStr;
    }

    /**
     * 得到加密,加签后参数
     */
    public static PtsPolicyEncryptDto getEncryptStr2(String jsonStr) throws Exception {
        //加密
        String content = AESGCMUtil.encrypt(jsonStr, AES_KEY);
        //加签
        String sign = AESGCMUtil.sign(content, SHA_PRIVATE_KEY);
        PtsPolicyEncryptDto encryptDto = new PtsPolicyEncryptDto();
        encryptDto.setContent(content);
        encryptDto.setSign(sign);
        return encryptDto;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(getDecryptStr("{\"content\":\"RZzORc3JGDbQkFjZhpmDYC4hMl6qPFJ35ekDXL60CGbyN8HxR+RxM9WdPWjRVcOjIPBivP9praMvDKjfhaKolexiqgJnTkoYYzZAEqoSEQrc0qW3L5G/fWIQ7OhUdZi/c0iPAwasLLJN7AD6qYl/WleeEDAsds2+UQyWVx3l+WZpLVkXn7Vb0bUoMZRPZxhlNpXiRQ9Go+STidPMLglXC/dJewbt87vA36t1qptFbHym6YtVGS2rbZqq57VkgJsF5lp3eBLeGK0TIFl6secnbwa2XWXHcwHzglR1y9Fkie+7G0K1GV87EWPdE9loTkF4JcH/FZtyGP5uvS5IPhKLTrexj5bUWTi4dlJ5hoHQ1xUOQSpJB24cw2fIGkR+4dK2wi2H/f9Dr4/wANp9X7cp8cpM9fvYWH4+SwT/ZNBpDnJhBPTVd31JtKnxlYmRHG8PFN03gvsOF0JLw58iRBF98N6FELPZ6hPdsxRY7AiqzW4hYPal04ddefD/xCCr7DW+SPiq4vjbZP/2Xn6K1v2uGF7FvNk0WEmfn2nHY/66Ki+b4GvAEEXaAbz8vnTXWaqlsJSIwJfmxXXHs7l1XApIjSPhKYM3ISCGsJSjNcUZr+aRbfLUU7ijGRCOLtgR/eTTu8/BHNy96icaU4WCY4+ZdReNAwyAL1iFMKbvJ0jutosQJQh/RRDj540JfsQOc7tU2Nt5hZMql3y+DKIqduBMAShomthkSrnbh21sX9Z0uyQYE0hyGRNfnbZXnNJFREBVLpL4KkJCeNm2D9PfjfEG+UXiOiqymKPLawE+IQdnvb4je3yIxLeS0nOX7IJoQi0AEkGrHzMF7kyMtMQUqdOtWrwfI+3ZAagDsfHOzqsaKhITqzUOffqgrkxKH51NXrIaA64m7FiVUw9pBS7039S3GxsqL88E9zRA0LsOMmM7Y0bRgRIjtUGUKCuYWLqy1BTgmPB4BYjdwyIT+6W84SeZ/SFerYAz/1WyHK27vUpvAIzd9GSpv5MIxcUpTFkDfYdUnukk4MlaEgJsXMu2iJXSou7biKiBIyKC1zgzOCjpzYyoJkGY9a/eActASu3Vo52J7rO23Q5/4l9nrRdgaAi8VBrOoq9IN80Okh24mMHaMAa4ZT9QHnsPfxOEz4aTkle6+qPEfko5xynsI/5cCEhOGcQXW+O8UjeBD/yGuaAz33Xx2DooRu8aQOflzrxTDhhmLeJfh5bFdKF5w7gN9ESZNWCYKGZkmRhIUaK7T+M+A9DGlJfVw8l5HSJVz8aUF7v67+fO3X8M3euNfcmkkPgLKR7sOcZ2pmhL11fMDFs/n3sMhqSGcL4oOneY7woKwBraFp8aqQ4F//e9qS3BFD4c6oBkZxEFOH0KdPF9rtU6m/N01afa5FasP+YKZC/cM6lWtg0eHD1mj3TNPQLEVDuY+HjbA/bGwnue6nqucz6fUDq8EoFXo91avglZvrNJwXvJ6rGGTf7hgTWwv8NRMq4NxVS0AYWsk8eTANRY6co4HWoRsvtk8CQFNdnLvTNl7cwUquBmL9BDPFvYxBe1CtLmprj3SeLOEtD6/9xZ8fC1hrUf2QxuPbgK8UyN0dAsdcYSDSP/GTFXcgMnZwutWtuUtqp5+BbAMrOg4I6t01XKtZ9BV3aPX0obeIQPMXDji2L7sVtuVYHhmWaZsmxTmWsehIifiB2hUqRpLj5pNlDqbs4XfLgho9dVTW3IFBtsUgr0JhYBYs6MN2ivbj7NXMFldHOGUeUkCjWKqp4E3/95UGoGvhsQ4fO/q2R6m9hTRMIilpDPocSFQjEF294yG4jiecfuRZPhtpo00V/2qO1yjPHdwsgIotmWT97p/jlMGJYUpUICFDgDqWByHx/mAr9ZlJrlzHvib/iUXkOVvp8DArvBa74IAgVPxD/rf8u8TNN3TjOIRE90PaxC5fyXNquVda/fnEveI4eGq1EhBVQXJDsu620lWL6c+3BlSHXGjvuWcFYxaJinbIxSJsX3d/iHYQ5SFqDbBkWnKQx4Ng6Zd6GDE4/qONEfM/asOWdWFcOtQjA9omMYZf35VEbbGlduE0PyB5sPizc3f1vKeBIr5ib/jIy7giC6xMtB3Uco8DEHSvKDl7vyZKtomw9wMTP2RC9IBahqCCxupqFbp3m+VRHAmfGCK/k95pGN2e3r4hcnLyRaZKY8F4Fr22ezZH6n6IXjjNXBjGxqphIoueiw2eZeYuo9LHfiiMo8fHFuNFpYL4S3gP1SyXy32KFLjwe7/4r9Lt/moS5eriuHBop3niEZY71lM5qVJKtmJtEeQtTz+lkHmj0gnTvnWxO2yYakcsIeBIwoofhg+poy9SEfhYXODr4bPx4/8ySfpE6BGBfqhrw7JZpWSZ/13Bv2GlZwKqYOxyhYAWh68Wu0rqtnnzNnsbHTownVuiVWcz1B2RLXLWVEQJimTRT5xM3WSMXnj9RZ8e8lQwe1v2XIBwBXrJQR7PWzVvzshzfiKAODDt/J3QVAy8pqPpYarkzKB003DRYA56PbXEpT4NPfnhnf4JfisNy5RhJ4XJMXLIycVNUx8TxTSBn9VgEUfV0SmwC39RTutYcpqXOCvDPp3mUlB7iMaCncdqEdjGw4IvW54EaTRzBkXt28ghlALJiU6gpr87k+ypq2XviFmwrXfOxna40DGXOoNPRUP++82GeYvnTFPrB/23ZzGMT0NdNfGMWkArWY5gvdsy+MNcrNmeRL35YaxaXToZlTi4HSBwmv9bOu0AUt0A49gmmugaNK7qG9d2lKbB3hiUXASrws3Xb52ArBxKyhSF9tVRb9LO2wU0SPxuXL/e8uTd6eT8RGDpdupOnmy+jSDNQF74R7V9Z7ZBFvDkMuCFzNQKjTIzAUHVeJTLELuW8fCqKAF0EkgWzF+FnIf5UW1aIYrWEFGvMA0C95mVxG9VkfdxM+epjDs/n4153FQ6+pjCXggp5rzwKexOJ5vaLFoYAIP0QpCiZuyDSH9/R2hDqKbWTuyt11ay2Qkbv/1bpm+107Pu0y4ClV30e3AOB+pwQ=\",\"sign\":\"lzTnFcjA6jn4beosZW10SM00t7UKtM4wWqv6BMGFWmuxgEi0h9l7vp1YjROvcoM8e/cuBMN725yJXB585l+6mA93atOsv9kF25cq44c0aZUjzhoDsoJnn3Yo01RbLphbCD5SuOtCPQ84A1pcXi3rSLuNi6IVEQjWoPW6SCcVX7Tdz/Tonmg6EXTu1gp9CdOsiZuxKvU0kNJi9Cv/W2oX3BhtwNMNv4Ahv+bLYk6QwoTkGNu9FDIRPsvt9UG6W3F845eldKrWNvzVCDAExpPhok8fkecnAGaQemL9/n/dSOntgOlg5D8NentQnjmzuIrdlo8Y6DUfpp8TXLxLBB96kUI+SpA4RC5PBKxBE3QXSHsVgLRlpmoyNd/CDaM3OzyFLP+JSVvT+2stgu++OhPTQwdXqv3T/StM13cmOOKUaX6olmBz540drwkBdEkdIYT1+G5X0QjnFcITFHYsLMMdWjLQLmakr+/vEDizSwQPePXcPjHh5rFrrtceL7W8riF+JLk5DsTIchXPWVdGbo20tDjYBGzPzBFMX3I/76neEB1cgy0rBfEmfoe8TngYvEv3zhWb/xhbbBrZc8+oa9yLfmyJSNANBezd6gVQ8MRWIOtzBAnJiwStWKHV7SMdpd/H8tkk2Bt9B7X4Dqy6A7ypcEONlrVgYHuKHYBYHuUMkoI=\"}"));

        System.out.println(getEncryptStr("{\"insuredInfos\":[{\"email\":\"1910592153@qq.com\",\"phone\":\"18166123797\",\"organName\":\"邵东市水东江镇邹龙烟花爆竹专卖店\",\"insuredId\":\"92430521MA4R6BCH7L\",\"address\":\"湖南省邵阳市邵东市水东江镇石牛村亦爱组8号旁\",\"insuredFirstIndustry\":\"烟花爆竹\",\"insuredSecondIndustry\":\"烟花爆竹销售\",\"insuredThirdIndustry\":\"烟花零售（专店）\",\"provinceCode\":\"430000\",\"cityCode\":\"430500\",\"districtCode\":\"430582\"}],\"insuranceNum\":\"12009023901747318453\",\"coInsurance\":\"1\",\"mainInsuranceCompany\":\"平安\",\"coInsuranceInfos\":[{\"insuranceCompany\":\"平安\",\"insuranceRate\":\"50\"},{\"insuranceCompany\":\"中国人民财产保险股份有限公司\",\"insuranceRate\":\"14\"},{\"insuranceCompany\":\"中国太平洋财产保险股份有限公司\",\"insuranceRate\":\"12\"},{\"insuranceCompany\":\"中华联合财产保险公司\",\"insuranceRate\":\"10\"},{\"insuranceCompany\":\"中国人寿财产保险股份有限公司\",\"insuranceRate\":\"8\"},{\"insuranceCompany\":\"阳光财产保险股份有限公司 \",\"insuranceRate\":\"6\"}],\"startTime\":\"2022-08-18\",\"endTime\":\"2023-08-17\",\"policyHolder\":\"邹龙\",\"policyHolderId\":\"430521199602157313\",\"insuredNum\":\"1\",\"mainRiskArticle\":\"平安产险（湖南）安全生产责任保险（2019版）\",\"totalSumInsured\":1.2E7,\"totalPremium\":700.0,\"perItemLimit\":[{\"dutyAttrAmountValue\":\"6000000\",\"dutyAttrCode\":\"197\",\"dutyAttributeName\":\"第三者人身伤亡赔偿限额\"},{\"dutyAttrAmountValue\":\"500000\",\"dutyAttrCode\":\"192\",\"dutyAttributeName\":\"每名雇员死亡赔偿限额\"},{\"dutyAttrAmountValue\":\"80000\",\"dutyAttrCode\":\"196\",\"dutyAttributeName\":\"法律服务费用赔偿限额\"},{\"dutyAttrAmountValue\":\"1000000\",\"dutyAttrCode\":\"191\",\"dutyAttributeName\":\"第三者财产损失赔偿限额\"},{\"dutyAttrAmountValue\":\"200000\",\"dutyAttrCode\":\"193\",\"dutyAttributeName\":\"第三者每人医疗费用限额\"},{\"dutyAttrAmountValue\":\"300000\",\"dutyAttrCode\":\"194\",\"dutyAttributeName\":\"事故鉴定费用赔偿限额\"},{\"dutyAttrAmountValue\":\"6000000\",\"dutyAttrCode\":\"17\",\"dutyAttributeName\":\"每次事故赔偿限额\"},{\"dutyAttrAmountValue\":\"12000000\",\"dutyAttrCode\":\"16\",\"dutyAttributeName\":\"累计赔偿限额\"},{\"dutyAttrAmountValue\":\"1000000\",\"dutyAttrCode\":\"195\",\"dutyAttributeName\":\"抢险救援费用赔偿限额\"}]}"));
    }

    /**
     * 解密,验签
     */
    public static String getDecryptStr(String content, String sign) throws Exception {
        if(StringUtils.isBlank(content) || StringUtils.isBlank(sign)){
            throw new BusinessException("sign.error");
        }
        //验签
        boolean verifyFlag = verify(content, sign, SHA_PUBLIC_KEY);
        if (!verifyFlag) {
            //验签失败,抛异常
            throw new BusinessException("sign.error");
        } else {
            //验签成功,继续解密
            String jsonStr = decrypt(content, AES_KEY);
            return jsonStr;
        }
    }

    /**
     * 解密,验签
     */
    public static String getDecryptStr(String jsonStr) throws Exception {
        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        String content = jsonObject.getString("content");
        String sign = jsonObject.getString("sign");
        if(StringUtils.isBlank(content) || StringUtils.isBlank(sign)){
            throw new BusinessException("sign.error");
        }
        //验签
        boolean verifyFlag = verify(content, sign, SHA_PUBLIC_KEY);
        if (!verifyFlag) {
            //验签失败,抛异常
            throw new BusinessException("sign.error");
        } else {
            //验签成功,继续解密
            String jsonStr2 = decrypt(content, AES_KEY);
            return jsonStr2;
        }
    }

    /**
     * 解密-GCM
     *
     * @param base64Content 密文
     * @param encryptPass   密钥
     * @return 明文
     */
    public static String decrypt(String base64Content, String encryptPass) {
        try {
            byte[] content = Base64.getDecoder().decode(base64Content);
            if (content.length < LEN_TWELVE + LEN_SIXTEEN) {
                throw new IllegalArgumentException();
            }
            GCMParameterSpec params = new GCMParameterSpec(KEY_SIZE, content, 0, LEN_TWELVE);
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(encryptPass), params);
            byte[] decryptData = cipher.doFinal(content, LEN_TWELVE, content.length - LEN_TWELVE);
            return new String(decryptData, CHARSET);
        } catch (Exception e) {
            log.error("decrypt error... {}", e);
        }
        return null;
    }

    /**
     * 生成SecretKeySpec
     *
     * @param encryptPass 初始密钥
     * @return SecretKeySpec
     * @throws NoSuchAlgorithmException
     */
    private static SecretKeySpec getSecretKey(String encryptPass) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_ALGORITHM);
        // 初始化密钥生成器，AES要求密钥长度为128位、192位、256位
        SecureRandom secureRandom = SecureRandom.getInstance(ALGORITHM_SHA1PRNG);
        secureRandom.setSeed(encryptPass.getBytes());
        keyGenerator.init(KEY_SIZE, secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();
        // 转换为AES专用密钥
        return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);
    }

    /**
     * 私钥加签
     *
     * @param content 入参
     * @return 签名值
     * @throws Exception
     */
    public static String sign(String content, String privateKey) throws Exception {
        byte[] contentBytes = content.getBytes(CHARSET);
        PrivateKey priKey = getPrivateKey(privateKey);
        Signature signature = Signature.getInstance(SIGNATURE_INSTANCE);
        signature.initSign(priKey);
        signature.update(contentBytes);
        byte[] signBytes = signature.sign();
        byte[] signBase64Bytes = Base64.getEncoder().encode(signBytes);
        return new String(signBase64Bytes, CHARSET);
    }

    /**
     * 密钥验签
     *
     * @param content 入参
     * @param sign    签名
     * @return 验签结果
     * @throws Exception
     */
    public static boolean verify(String content, String sign, String publicKey) throws Exception {
        PublicKey pubKey = getPublicKey(publicKey);
        Signature signature = Signature.getInstance(SIGNATURE_INSTANCE);
        signature.initVerify(pubKey);
        signature.update(content.getBytes(CHARSET));
        return signature.verify(Base64.getDecoder().decode(sign));
    }

    /**
     * 私钥字符串转PrivateKey实例-仅用于加验签
     *
     * @param privateKey 私钥字符串
     * @return PrivateKey实例
     * @throws Exception
     */
    public static PrivateKey getPrivateKey(String privateKey) throws Exception {
        byte[] privateKeyBytes = Base64.getDecoder().decode(privateKey.getBytes(CHARSET));
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
        return keyFactory.generatePrivate(keySpec);
    }

    /**
     * 公钥字符串转PublicKey实例-仅用于加验签
     *
     * @param publicKey 公钥字符串
     * @return PublicKey实例
     * @throws Exception
     */
    public static PublicKey getPublicKey(String publicKey) throws Exception {
        byte[] publicKeyBytes = Base64.getDecoder().decode(publicKey.getBytes(CHARSET));
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
        return keyFactory.generatePublic(keySpec);
    }
}


