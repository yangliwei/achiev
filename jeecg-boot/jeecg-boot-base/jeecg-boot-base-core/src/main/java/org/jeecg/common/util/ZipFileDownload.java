package org.jeecg.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

/**
 * 下载zip文件
 */
@Slf4j
public class ZipFileDownload {

    public static int NUM = 1;

    public static void plistDownLoad(HttpServletResponse response, List<String> lists) throws Exception {

        // 此处模拟处理ids,拿到文件下载url
        if (lists.size() != 0) {
            // 创建临时路径,存放压缩文件
            String zipFilePath = "C:\\Users\\admin\\Desktop\\zip.zip";
            // 压缩输出流,包装流,将临时文件输出流包装成压缩流,将所有文件输出到这里,打成zip包
            ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath));
            // 循环调用压缩文件方法,将一个一个需要下载的文件打入压缩文件包
            for (String list : lists) {
                fileToZip(list, zipOut);
            }
            // 压缩完成后,关闭压缩流  ·
            zipOut.close();
            //拼接下载默认名称并转为ISO-8859-1格式
            String fileName = new String(("我的压缩文件.zip").getBytes(), StandardCharsets.UTF_8);
            response.setHeader("Content-Disposition", "attchment;filename=" + fileName);
            //该流不可以手动关闭,手动关闭下载会出问题,下载完成后会自动关闭
            ServletOutputStream outputStream = response.getOutputStream();
            FileInputStream inputStream = new FileInputStream(zipFilePath);
            // 如果是SpringBoot框架,在这个路径
            // org.apache.tomcat.util.http.fileupload.IOUtils产品
            // 否则需要自主引入apache的 commons-io依赖
            // copy方法为文件复制,在这里直接实现了下载效果
            IOUtils.copy(inputStream, outputStream);
            // 关闭输入流
            inputStream.close();
            //下载完成之后，删掉这个zip包
            File fileTempZip = new File(zipFilePath);
            fileTempZip.delete();
        }
    }


    public static void fileToZip(String filePath, ZipOutputStream zipOut) throws IOException {
        // 需要压缩的文件
        File file = new File(filePath);
        // 获取文件名称,如果有特殊命名需求,可以将参数列表拓展,传fileName
        String fileName = file.getName();
        URL url = null;
        try {
            url = new URL(filePath);
        } catch (MalformedURLException e) {
            filePath = "https:" + filePath;
            url = new URL(filePath);
        }
        URLConnection conn = url.openConnection();
        InputStream inputStream = conn.getInputStream();
//        FileInputStream fileInput = new FileInputStream(filePath);
        // 缓冲
        byte[] bufferArea = new byte[1024 * 10];
        BufferedInputStream bufferStream = new BufferedInputStream(inputStream, 1024 * 10);
        // 将当前文件作为一个zip实体写入压缩流,fileName代表压缩文件中的文件名称
        try {
            zipOut.putNextEntry(new ZipEntry(fileName));
        } catch (ZipException z) {
            String[] split = fileName.split(".docx");
            zipOut.putNextEntry(new ZipEntry(split[0] + "(" + NUM + ").docx"));
        }
        NUM = NUM + 1;
        int length = 0;
        // 最常规IO操作,不必紧张
        while ((length = bufferStream.read(bufferArea, 0, 1024 * 10)) != -1) {
            zipOut.write(bufferArea, 0, length);
        }
        //关闭流
        inputStream.close();
        // 需要注意的是缓冲流必须要关闭流,否则输出无效
        bufferStream.close();
        // 压缩流不必关闭,使用完后再关
    }

}
