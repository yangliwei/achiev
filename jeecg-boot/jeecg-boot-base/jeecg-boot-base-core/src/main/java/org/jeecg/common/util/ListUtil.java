package org.jeecg.common.util;

import java.util.List;

/**
 * 优化风控建议书的一些工具方法
 */
public class ListUtil {

    //List<List<String>>转String[][]
    public static String[][] ListsToArrays(List<List<String>> lists) {
        String[][] strArr = new String[lists.size()][];
        for (int i = 0; i < lists.size(); i++) {
            if (lists.get(i).size() < lists.get(0).size()) {
                int number = lists.get(0).size() - lists.get(i).size();
                for (int j = 0; j < number; j++) {
                    lists.get(i).add(lists.get(i).size(), String.valueOf(0));
                }
            }
            strArr[i] = lists.get(i).toArray(new String[0]);
        }
        return strArr;
    }

}
