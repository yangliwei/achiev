package org.jeecg;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.PasswordUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.achievscheme.entity.AchievScheme;
import org.jeecg.modules.achievscheme.service.IAchievSchemeService;
import org.jeecg.modules.demo.mock.MockController;
import org.jeecg.modules.demo.test.entity.JeecgDemo;
import org.jeecg.modules.demo.test.mapper.JeecgDemoMapper;
import org.jeecg.modules.demo.test.service.IJeecgDemoService;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysUserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,classes = JeecgSystemApplication.class)
public class SampleTest {

	@Resource
	private JeecgDemoMapper jeecgDemoMapper;
	@Resource
	private IJeecgDemoService jeecgDemoService;
//	@Resource
//	private ISysDataLogService sysDataLogService;
	@Resource
	private MockController mock;
	@Autowired
	private ISysUserService sysUserService;

	@Autowired
	private IAchievSchemeService achievSchemeService;

    @Test
    public void test1002() throws ParseException {
		//当月最后一天
		LocalDateTime midnight = LocalDateTime.now().plusMonths(1).withDayOfMonth(1).withHour(0)
				.withMinute(0).withSecond(0).withNano(0);
		System.out.println(ChronoUnit.SECONDS.between(LocalDateTime.now(), midnight));
	}

	@Test
    public void test1001() throws Exception {
    	// 初始密码
		String salt = "PB8Ojp9e";
		List<SysUser> list = sysUserService.list();
		for (SysUser sysUser : list) {
			String username = sysUser.getUsername();
			if ("admin".equals(username)) {
				continue;
			}
			String a = username.substring(0, 3);
			String b = username.substring(7, 11);
			String x = a + "1111" + b;
			String passwordEncode = PasswordUtil.encrypt(x, "123456",salt);
			SysUser user = new SysUser();
			user.setId(sysUser.getId());
			user.setUsername(x);
			user.setWorkNo(x);
			user.setPassword(passwordEncode);
			sysUserService.updateById(user);
		}

	}

	@Test
	public void testSelect() {
		System.out.println(("----- selectAll method test ------"));
		List<JeecgDemo> userList = jeecgDemoMapper.selectList(null);
		Assert.assertEquals(5, userList.size());
		userList.forEach(System.out::println);
	}

	@Test
	public void testXmlSql() {
		System.out.println(("----- selectAll method test ------"));
		List<JeecgDemo> userList = jeecgDemoMapper.getDemoByName("Sandy12");
		userList.forEach(System.out::println);
	}

	/**
	 * 测试事务
	 */
	@Test
	public void testTran() {
		jeecgDemoService.testTran();
	}

	//author:lvdandan-----date：20190315---for:添加数据日志测试----
	/**
	 * 测试数据日志添加
	 */
//	@Test
//	public void testDataLogSave() {
//		System.out.println(("----- datalog test ------"));
//		String tableName = "jeecg_demo";
//		String dataId = "4028ef81550c1a7901550c1cd6e70001";
//		String dataContent = mock.sysDataLogJson();
//		sysDataLogService.addDataLog(tableName, dataId, dataContent);
//	}
	//author:lvdandan-----date：20190315---for:添加数据日志测试----
}
