package org.jeecg.modules.nanliattach.service;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.modules.nanliattach.entity.NanliAttach;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 附件表
 * @Author: risk-manage
 * @Date:   2021-10-27
 * @Version: V1.0
 */
public interface INanliAttachService extends IService<NanliAttach> {


    /**
     * 通过id查询文件地址
     * @param id
     * @return
     */
    public String getUrlById(String id);

    /**
     * 通过ids查URLlist
     * @param ids
     * @return
     */
    public List<JSONObject> getUrlList(String ids);

    public List<String> getByUrlList(String[] urls);
}
