package org.jeecg.modules.assesscontent.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.assesscontent.entity.AssessContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 考评内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface AssessContentMapper extends BaseMapper<AssessContent> {

    void updateAssessContentAvg(@Param("id") String id);

    void updateBatchAssessContentAvg(@Param("list") List<String> list);

    List<AssessContent> queryWithoutScoreDataList();

}
