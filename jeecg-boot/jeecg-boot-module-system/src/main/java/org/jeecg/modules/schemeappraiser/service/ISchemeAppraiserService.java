package org.jeecg.modules.schemeappraiser.service;

import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * @Description: 方案考评人表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface ISchemeAppraiserService extends IService<SchemeAppraiser> {

    /**
     * 查询对应schemeId下的考评人
     * @param id
     * @return
     */
    List<SchemeAppraiser> querySchemeSchemeAppraiserBySchemeId(String id);

    List<SchemeAppraiser> queryNotDeletedSchemeAppraiser();

    List<SchemeAppraiser> listByAppraiser(String id);
}
