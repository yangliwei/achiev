package org.jeecg.modules.assesscontent.entity.vo;

import lombok.Data;

import java.util.List;

@Data
public class AssessContentGroup{
    private String schemeContent;
    private List<AssessContentVo> assessContentVoList;
}
