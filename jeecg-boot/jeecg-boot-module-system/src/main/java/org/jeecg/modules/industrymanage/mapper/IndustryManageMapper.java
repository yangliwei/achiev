package org.jeecg.modules.industrymanage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.industrymanage.entity.IndustryManage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 行业管理
 * @Author: jeecg-boot
 * @Date:   2021-07-04
 * @Version: V1.0
 */
public interface IndustryManageMapper extends BaseMapper<IndustryManage> {

}
