package org.jeecg.modules.achievassess.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.achievassess.entity.AchievAssess;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessBo;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessVo;
import org.jeecg.modules.achievassess.mapper.AchievAssessMapper;
import org.jeecg.modules.achievassess.service.IAchievAssessService;
import org.jeecg.modules.achievscheme.entity.AchievScheme;
import org.jeecg.modules.achievscheme.service.IAchievSchemeService;
import org.jeecg.modules.assesscontent.entity.AssessContent;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentGroup;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentVo;
import org.jeecg.modules.assesscontent.service.IAssessContentService;
import org.jeecg.modules.assessdescription.entity.AssessDescription;
import org.jeecg.modules.assessdescription.service.IAssessDescriptionService;
import org.jeecg.modules.assessscore.entity.AssessScore;
import org.jeecg.modules.assessscore.service.IAssessScoreService;
import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import org.jeecg.modules.schemeappraiser.service.ISchemeAppraiserService;
import org.jeecg.modules.schemecontent.entity.SchemeContent;
import org.jeecg.modules.schemecontent.service.ISchemeContentService;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import org.jeecg.modules.schemedescription.service.ISchemeDescriptionService;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.entity.SysUserDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecg.modules.system.service.ISysUserDepartService;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Description: 绩效考评总表
 * @Author: jeecg-boot
 * @Date: 2023-01-16
 * @Version: V1.0
 */

@Service
public class AchievAssessServiceImpl extends ServiceImpl<AchievAssessMapper, AchievAssess> implements IAchievAssessService {


    @Autowired
    private IAchievSchemeService achievSchemeService;
    @Autowired
    private ISchemeContentService schemeContentService;
    @Autowired
    private ISchemeDescriptionService schemeDescriptionService;
    @Autowired
    private IAssessDescriptionService assessDescriptionService;
    @Autowired
    private ISchemeAppraiserService schemeAppraiserService;
    @Autowired
    private IAssessContentService assessContentService;
    @Autowired
    private IAssessScoreService assessScoreService;
    @Autowired
    private ISysDepartService sysDepartService;
    @Autowired
    private ISysUserDepartService sysUserDepartService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private RedisTemplate redisTemplate;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void generatePerformanceAppraisal() {
        // 预先查询能提前处理的数据
        // 部门信息
        List<SysDepart> sysDepartList = sysDepartService.list(new LambdaQueryWrapper<SysDepart>().isNotNull(SysDepart::getSchemeId));
        if (ObjectUtils.isNotEmpty(sysDepartList)) {
            List<String> departIdList = sysDepartList.stream().map(SysDepart::getId).collect(Collectors.toList());
            // 用户绑定的部门
            List<SysUserDepart> userDepartList = sysUserDepartService.list(new LambdaQueryWrapper<SysUserDepart>().in(SysUserDepart::getDepId, departIdList));
            // 已经被删除的用户, 用于跳过生成
            List<String> deletedUserIdList = userService.queryDeletedUserIdList();
            // <部门id, 该部门id下的用户id>
            Map<String, List<String>> userDepartMap = userDepartList.stream().collect(Collectors.groupingBy(SysUserDepart::getDepId, Collectors.mapping(SysUserDepart::getUserId, Collectors.toList())));
            // 用户信息, <用户id, 用户名>
            Map<String, String> userMap = userService.list(new LambdaQueryWrapper<SysUser>().in(SysUser::getId, userDepartList.stream().map(SysUserDepart::getUserId).collect(Collectors.toList()))).stream().collect(Collectors.toMap(SysUser::getId, SysUser::getRealname));
            // 方案考评内容 <方案id, 该方案id下的所有方案考评内容信息>
            Map<String, List<SchemeContent>> schemeContentMap = schemeContentService.queryNotDeletedSchemeContent().stream().collect(Collectors.groupingBy(SchemeContent::getSchemeId));
            // 程度描述内容 <考评内容id, 该考评内容下的所有程度描述>
            Map<String, List<SchemeDescription>> schemeDescriptionMap = schemeDescriptionService.queryNotDeletedSchemeDescriptionType2().stream().collect(Collectors.groupingBy(SchemeDescription::getSchemeContentId));
            // 考评人 <考评内容id, 该方案id下的所有考评人信息>
            Map<String, List<SchemeAppraiser>> schemeAppraiserMap = schemeAppraiserService.queryNotDeletedSchemeAppraiser().stream().collect(Collectors.groupingBy(SchemeAppraiser::getSchemeContentId));

            // 2月份只会生成2月的信息, 3月份只会生成3月份的信息, 与定时任务的设定时间无关
            String format = new SimpleDateFormat("yyyy-MM").format(new Date());
            // 绩效考评信息 <被考评人id, 绩效考评信息>
            Map<String, List<AchievAssess>> achievAssessMap = this.list(new LambdaQueryWrapper<AchievAssess>().likeRight(AchievAssess::getCreateTime, format)).stream().collect(Collectors.groupingBy(AchievAssess::getAssessAppraisee));
            // 绩效考评内容信息 <考评id, 绩效考评内容信息>
            Map<String, List<AssessContent>> assessContentMap = assessContentService.list(new LambdaQueryWrapper<AssessContent>().likeRight(AssessContent::getCreateTime, format)).stream().collect(Collectors.groupingBy(AssessContent::getAssessId));
            // 绩效描述程度 <考评内容id, 绩效描述程度>
            Map<String, List<AssessDescription>> assessDescriptionMap = assessDescriptionService.list(new LambdaQueryWrapper<AssessDescription>().likeRight(AssessDescription::getCreateTime, format)).stream().collect(Collectors.groupingBy(AssessDescription::getAssessContentId));
            // 绩效评分 <考评内容id, 绩效评分>
            Map<String, List<AssessScore>> assessScoreMap = assessScoreService.list(new LambdaQueryWrapper<AssessScore>().likeRight(AssessScore::getCreateTime, format)).stream().collect(Collectors.groupingBy(AssessScore::getAssessContentId));

            // 一个一个部门进行生成 -------------------------
            for (SysDepart sysDepart : sysDepartList) {
                log.warn(sysDepart.getDepartName() + "__绩效生成开始");
                // 方案id
                String schemeId = sysDepart.getSchemeId();
                // 部门id
                String departId = sysDepart.getId();
                // 得到该部门底下的所有员工
                List<String> userIdList = userDepartMap.get(departId);
                if (ObjectUtils.isEmpty(userIdList)) {
                    log.warn(sysDepart.getDepartName() + "__无员工, 不生成");
                    continue;
                }
                // 人员-考评总表数据处理
                for (String userId : userIdList) {
                    // 已被删除的员工, 跳过生成
                    if (deletedUserIdList.contains(userId)) {
                        continue;
                    }
                    // 封装所需的数据
                    AchievAssess achievAssess = new AchievAssess();
                    achievAssess.setSchemeId(schemeId);
                    achievAssess.setAssessAppraisee(userId);
                    achievAssess.setAssessAppraiseeName(userMap.get(userId));
                    // 如果该用户本月已经生成过了绩效考评总表的数据, 则不再生成总表数据
                    if (ObjectUtils.isEmpty(achievAssessMap.get(userId))) {
                        // 如果没有生成过, 则进行生成
                        this.save(achievAssess);
                        // 将新增的数据添加到预查数据中
                        List<AchievAssess> achievAssessList = new ArrayList<>();
                        achievAssessList.add(achievAssess);
                        achievAssessMap.put(userId, achievAssessList);
                    } else {
                        List<String> appraiseeSchemeList = achievAssessMap.get(userId).stream().map(AchievAssess::getSchemeId).collect(Collectors.toList());
                        // 如果本月内已经存在该用户名和方案id的数据, 则不进行添加
                        if (appraiseeSchemeList.contains(schemeId)) {
                            achievAssess.setId(achievAssessMap.get(userId).stream().filter(item -> item.getSchemeId().equals(schemeId)).findFirst().get().getId());
                        } else {
                            // 如果匹配不上, 则还进行生成
                            this.save(achievAssess);
                            List<AchievAssess> achievAssessList = achievAssessMap.get(userId);
                            achievAssessList.add(achievAssess);
                            achievAssessMap.put(userId, achievAssessList);
                        }
                    }
                    // 生成绩效考评内容数据 -------------------------
                    List<SchemeContent> schemeContentList = schemeContentMap.get(schemeId);
                    for (SchemeContent schemeContent : schemeContentList) {
                        // 人员-考评内容表数据处理
                        // 方案内容id
                        String schemeContentId = schemeContent.getId();
                        // 考评id
                        String achievAssessId = achievAssess.getId();
                        AssessContent assessContent = new AssessContent();
                        assessContent.setAssessId(achievAssessId);
                        assessContent.setSchemeContentId(schemeContentId);
                        // 如果本月内不存在该考评id的绩效考评内容, 则进行添加
                        if (ObjectUtils.isEmpty(assessContentMap.get(achievAssessId))) {
                            assessContentService.save(assessContent);
                            List<AssessContent> assessContentList = new ArrayList<>();
                            assessContentList.add(assessContent);
                            assessContentMap.put(achievAssessId, assessContentList);
                        } else {
                            List<String> contentSchemeList = assessContentMap.get(achievAssessId).stream().map(AssessContent::getSchemeContentId).collect(Collectors.toList());
                            // 如果本月内已经存在该考评id和方案内容id的数据, 则不进行添加
                            if (contentSchemeList.contains(schemeContentId)) {
                                assessContent.setId(assessContentMap.get(achievAssessId).stream().filter(item -> item.getSchemeContentId().equals(schemeContentId)).findFirst().get().getId());
                            } else {
                                // 没匹配上再重新添加
                                assessContentService.save(assessContent);
                                List<AssessContent> assessContentList = assessContentMap.get(achievAssessId);
                                assessContentList.add(assessContent);
                                assessContentMap.put(achievAssessId, assessContentList);
                            }
                        }
                        String assessContentId = assessContent.getId();
                        // 生成考评描述 -------------------------
                        List<SchemeDescription> schemeDescriptionList = ObjectUtils.isEmpty(schemeDescriptionMap.get(schemeContentId)) ? new ArrayList<>() : schemeDescriptionMap.get(schemeContentId);
                        for (SchemeDescription schemeDescription : schemeDescriptionList) {
                            String schemeDescriptionId = schemeDescription.getId();
                            assessDescriptionService.generateAssessDescription(schemeDescriptionId, assessContentId, achievAssessId, assessDescriptionMap);
                        }
                        // 生成考评人 -------------------------
                        List<SchemeAppraiser> schemeAppraiserList = ObjectUtils.isEmpty(schemeAppraiserMap.get(schemeContentId)) ? new ArrayList<>() : schemeAppraiserMap.get(schemeContentId);
                        for (SchemeAppraiser schemeAppraiser : schemeAppraiserList) {
                            // 人员-评分表数据处理
                            String schemeAppraiserId = schemeAppraiser.getSchemeAppraiser();
                            String schemeAppraiserName = schemeAppraiser.getSchemeAppraiserName();
                            // 如果是特殊考评人, 则进行特殊化处理
                            if (StringUtils.isBlank(schemeAppraiserId) && StringUtils.isNotBlank(schemeAppraiserName)) {
                                List<SysUser> specialAppraiserList = assessScoreService.querySpecialAppraiser(userId, schemeAppraiserName, departId);
                                for (SysUser specialAppraiser : specialAppraiserList) {
                                    String specialAppraiserId = specialAppraiser.getId();
                                    String specialAppraiserName = specialAppraiser.getRealname();
                                    assessScoreService.generateAssessScore(achievAssessId, assessContentId, specialAppraiserId, specialAppraiserName, assessScoreMap, userId);
                                }
                            } else if (StringUtils.isNotBlank(schemeAppraiserId)) {
                                // 如果不是, 并且考评人id不为空, 则进行生成
                                assessScoreService.generateAssessScore(achievAssessId, assessContentId, schemeAppraiserId, schemeAppraiserName, assessScoreMap, userId);
                            }
                        }
                    }
                }
                log.warn(sysDepart.getDepartName() + "__绩效生成结束");
            }
            redisTemplate.opsForValue().set("sys:cache:dict::data_is_FinishAssess", "1", this.getLastDaySeconds(), TimeUnit.SECONDS);
            redisTemplate.opsForValue().set("sys:cache:dict::last_data_month", format);
        }
        // 追加生成考评内容只有一个考评人且为自己的数据
        List<AssessContent> withoutScoreList = assessContentService.queryWithoutScoreDataList();
        if (ObjectUtils.isNotEmpty(withoutScoreList)) {
            List<AssessScore> dataList = new ArrayList<>();
            for (AssessContent assessContent : withoutScoreList) {
                String assessContentId = assessContent.getId();
                String assessId = assessContent.getAssessId();
                AssessScore assessScore = new AssessScore();
                assessScore.setAssessId(assessId);
                assessScore.setCreateTime(assessContent.getCreateTime());
                assessScore.setAssessContentId(assessContentId);
                assessScore.setScoreAppraiserName("考评人为自己, 默认满分");
                assessScore.setScoreAppraiser("");
                dataList.add(assessScore);
            }
            assessScoreService.saveBatch(dataList);
        }
        log.warn("+_____绩效生成结束!_____+");
    }

    @Override
    public AchievAssessVo queryAssessById(String id) {
        AchievAssess achievAssess = this.getById(id);
        AchievAssessVo achievAssessVo = new AchievAssessVo();
        BeanUtils.copyProperties(achievAssess, achievAssessVo);
        String schemeId = achievAssessVo.getSchemeId();
        AchievScheme achievScheme = achievSchemeService.getById(schemeId);
        achievAssessVo.setSchemeName(achievScheme.getSchemeName());
        achievAssessVo.setCompletedContent(achievScheme.getCompletedContent());
        achievAssessVo.setShouldCompletedContent(achievScheme.getShouldCompletedContent());
        achievAssessVo.setActualCompletedContent(achievScheme.getActualCompletedContent());
        achievAssessVo.setCompletedAppraiser(achievScheme.getCompletedAppraiser());
        achievAssessVo.setCompletedAppraiserName(achievScheme.getCompletedAppraiserName());
        // 封装考评内容的数据
        List<AssessContentVo> assessContentVoList = assessContentService.queryAssessContentVoByAssessId(id, schemeId);
        achievAssessVo.setAssessContentGroup(assessContentService.AssessContentListGroup(assessContentVoList));
        return achievAssessVo;
    }
    @Override
    public AchievAssessVo queryAssessByIdAndUserId(String id, String userId) {
        AchievAssess achievAssess = this.getById(id);
        AchievAssessVo achievAssessVo = new AchievAssessVo();
        BeanUtils.copyProperties(achievAssess, achievAssessVo);
        String schemeId = achievAssessVo.getSchemeId();
        AchievScheme achievScheme = achievSchemeService.getById(schemeId);
        achievAssessVo.setSchemeName(achievScheme.getSchemeName());
        // 封装考评内容的数据
        List<AssessContentVo> assessContentVoList = assessContentService.queryAssessContentVoByAssessId(id, schemeId, userId);
        achievAssessVo.setAssessContentGroup(assessContentService.AssessContentListGroup(assessContentVoList));
        achievAssessVo.setScoringCount(null);
        achievAssessVo.setAssessTotal(null);
        return achievAssessVo;
    }

    @Override
    public IPage<AchievAssessVo> queryResponsibleData(Page<AchievAssessVo> page, String userId, String userName, String schemeName, String date) {
        IPage<AchievAssessVo> achievAssessVoIPage = baseMapper.queryResponsibleData(page, userId, userName, schemeName, date);
        List<AchievAssessVo> records = achievAssessVoIPage.getRecords();
        if (ObjectUtils.isNotEmpty(records)) {
            List<String> collect = sysUserDepartService.list(new LambdaQueryWrapper<SysUserDepart>().eq(SysUserDepart::getUserId, userId)).stream().map(SysUserDepart::getDepId).collect(Collectors.toList());
            List<String> tempList = new ArrayList<>(collect);
            sysDepartService.queryAllSubordinates(collect);
            collect.removeAll(tempList);
            for (AchievAssessVo record : records) {
                if (!Objects.equals(record.getDepartId(), "") && !collect.contains(record.getDepartId())) {
                    record.setScoringCount(null);
                    record.setAssessTotal(null);
                }
            }
        }
        return achievAssessVoIPage;
    }

    @Override
    public IPage<AchievAssessVo> querySchemeByAppraisee(Page<AchievAssessVo> page, String userId, String date, String userName, String schemeName) {
        return baseMapper.querySchemeByAppraisee(page, userId, date, userName, schemeName);
    }

    @Override
    public IPage<AchievAssessVo> queryCompletionRate(Page<AchievAssessVo> page, String userId, String date, String name) {
        return baseMapper.queryCompletionRate(page, userId, date, name);
    }

    @Override
    public AchievAssessVo queryCompletionRateInfo(String id) {
        return baseMapper.queryCompletionRateInfo(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAssessScore(AchievAssessVo achievAssessVo) {
        JudgmentData(achievAssessVo);

        List<AssessScore> assessScoreList = new ArrayList<>();
        List<AssessDescription> assessDescriptionList = new ArrayList<>();
        // 提取页面返回的数据进行修改
        List<AssessContentVo> AssessContentVoList = new ArrayList<>();
        for (AssessContentGroup contentGroup : achievAssessVo.getAssessContentGroup()) {
            AssessContentVoList.addAll(contentGroup.getAssessContentVoList());
        }
        for (AssessContentVo assessContentVo : AssessContentVoList) {
            assessDescriptionList.addAll(assessContentVo.getAssessDescriptionList().stream().map(item -> {
                AssessDescription assessDescription = new AssessDescription();
                BeanUtils.copyProperties(item, assessDescription);
                if (ObjectUtils.isNotEmpty(assessDescription.getContentCompletionRate()) && assessDescription.getContentCompletionRate().isNaN()) {
                    assessDescription.setContentCompletionRate(null);
                }
                return assessDescription;
            }).collect(Collectors.toList()));
            Map<String, List<AssessDescription>> rateMap = new HashMap<>();
            if (ObjectUtils.isNotEmpty(assessDescriptionList)) {
                rateMap = assessDescriptionList.stream()
                        .filter(item -> ObjectUtils.isNotEmpty(item.getContentCompletionRate()))
                        .collect(Collectors.groupingBy(AssessDescription::getAssessContentId));
            }
            // 取到对应考核内容的分值上限
            String schemeContent = assessContentVo.getSchemeContent();
            int i = schemeContent.lastIndexOf("（") + 1;
            int j = schemeContent.lastIndexOf("）");
            BigDecimal max = new BigDecimal(schemeContent.substring(i, j));
            List<AssessScore> scoreList = assessContentVo.getAssessScoreList();
            assessScoreList.addAll(scoreList);
            for (AssessScore assessScore : scoreList) {
                Double rate = null;
                // 计算模式二的分值, 按照完成率来计算
                if (ObjectUtils.isNotEmpty(rateMap.get(assessScore.getAssessContentId()))) {
                    rate = 0.0;
                    for (AssessDescription assessDescription : rateMap.get(assessScore.getAssessContentId())) {
                        rate += assessDescription.getContentCompletionRate();
                    }
                }
                if (ObjectUtils.isNotEmpty(rate)) {
                    BigDecimal rateBd = new BigDecimal(rate + "")
                            // 除该考核内容内的完成率条数, 保留小数点两位 -> 285.75 / 3 = 95.25
                            .divide(BigDecimal.valueOf(rateMap.get(assessScore.getAssessContentId()).size()), 2, RoundingMode.CEILING)
                            // 再得到百分比 -> 95.25 / 100 = 0.9525
                            .divide(BigDecimal.valueOf(100), 4, RoundingMode.CEILING);
                    // 考核内容最大分乘以该百分比, 得到应得分 -> 30 * 0.9525 =  28.575 (数据库内自动保留两位小数)
                    BigDecimal v = max.multiply(rateBd);
                    assessScore.setScoreTotal(v.min(max).doubleValue());
                } else {
                    Double scoreTotal = assessScore.getScoreTotal();
                    if (ObjectUtils.isNotEmpty(scoreTotal) && scoreTotal > max.doubleValue()) {
                        throw new RuntimeException(schemeContent + "的单条分值不能超过" + max);
                    }
                }
            }
        }
        assessScoreService.updateBatchById(assessScoreList);
        assessDescriptionService.updateBatchById(assessDescriptionList);
    }

    @Override
    public void updateAssessTotal(String id) {

    }

    @Override
    public void updateAssessContentAvg(String id) {
        assessContentService.updateAssessContentAvg(id);
        baseMapper.updateBatchAssessTotal(Collections.singletonList(id));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateBatchAssessContentAvg(List<String> list) {
        if (ObjectUtils.isEmpty(list)) {
            return;
        }
        assessContentService.updateBatchAssessContentAvg(list);
        baseMapper.updateBatchAssessTotal(list);
    }

    @Override
    public void updateCompletionRate(AchievAssess achievAssess) {
        JudgmentData(achievAssess);
        this.updateById(achievAssess);
        baseMapper.updateBatchAssessTotal(Collections.singletonList(achievAssess.getId()));
    }

    // 判断数据, 是否满足要求
    private void JudgmentData(AchievAssess achievAssess) {
        if (!baseMapper.queryAssessIsLast(achievAssess.getId())) {
            throw new RuntimeException("该考评表不是最新的考评表, 不允许修改!");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar instance = Calendar.getInstance();
        instance.setTime(achievAssess.getCreateTime());
        instance.add(Calendar.MONTH, 1);
        Date time = instance.getTime();
        String nowMonth = sdf.format(new Date());
        String dataMonth = sdf.format(achievAssess.getCreateTime());
        String dataNextMonth = sdf.format(time);
        if (!dataNextMonth.equals(nowMonth)) {
//            throw new RuntimeException("未到" + dataNextMonth + "月, 不允许考评" + dataMonth + "月的数据"); TODO huang
        }
    }

    @Override
    public List<AchievAssessBo> getAssessAppraiseeNameAndScoringCount(String date) {
        return baseMapper.queryAssessAppraiseeNameAndScoringCount(date);
    }

    @Override
    public boolean isFinishAssess() {
        Object redisData = redisTemplate.opsForValue().get("sys:cache:dict::data_is_FinishAssess");
        if (ObjectUtils.isNotEmpty(redisData)) {
            return true;
        }
        AchievAssess achievAssess = this.getOne(new LambdaQueryWrapper<AchievAssess>().orderByDesc(AchievAssess::getCreateTime).last("LIMIT 1"));
        if (ObjectUtils.isNotEmpty(achievAssess)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            String dataMonth = sdf.format(achievAssess.getCreateTime());
            String nowMonth = sdf.format(new Date());
            if (nowMonth.equals(dataMonth)) {
                redisTemplate.opsForValue().set("sys:cache:dict::data_is_FinishAssess", "1", this.getLastDaySeconds(), TimeUnit.SECONDS);
                return true;
            }
        }
        return false;
    }

    @Override
    public String queryLastDataMonth() {
        Object redisData = redisTemplate.opsForValue().get("sys:cache:dict::last_data_month");
        if (ObjectUtils.isNotEmpty(redisData)) {
            return (String) redisData;
        }
        Date createTime = this.getOne(new LambdaQueryWrapper<AchievAssess>().orderByDesc(AchievAssess::getCreateTime).last("LIMIT 1")).getCreateTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        String format = sdf.format(createTime);
        redisTemplate.opsForValue().set("sys:cache:dict::last_data_month", format);
        return format;
    }

    /**
     * 获取现在到下个月的0点的秒数
     */
    public long getLastDaySeconds(){
        //当月最后一天
        LocalDateTime midnight = LocalDateTime.now().plusMonths(1).withDayOfMonth(1).withHour(0)
                .withMinute(0).withSecond(0).withNano(0);
        return ChronoUnit.SECONDS.between(LocalDateTime.now(),midnight);
    }
}

