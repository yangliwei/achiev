package org.jeecg.modules.pdf.util;

import com.aspose.words.*;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PdfUtil {

    /**
     * 破解Aspose中的水印
     */
    public static boolean getWordLicense() {
        boolean result = false;
        try {
            InputStream is = PdfUtil.class.getClassLoader().getResourceAsStream("license.xml");
            com.aspose.words.License aposeLic = new com.aspose.words.License();
            aposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 打印信息
     *
     * @param inPath
     * @param outPath
     * @param now
     * @param old
     */
    public static void outMessage(String inPath, String outPath, long now, long old) {
        double time = (now - old) / 1000.0;
        System.out.println("源文件：" + inPath);
        System.out.println("现文件：" + outPath);
        System.out.println("转换成功,共耗时：" + time + "秒");
    }

    /**
     * word转PDF
     * @param inPath 文档路径
     * @param outPath  输出路径
     */
    public static void docToPdf(String inPath, String outPath) {
        //不进行破解的话会在文档开头和结尾添加水印
        if (!getWordLicense()) {
            // 验证License 若不验证则转化出的pdf文档会有水印产生
            System.out.println(inPath + ",解析水印失败，请重试");
            return;
        }
        try {
            //获取开始时间
            long old = System.currentTimeMillis();
            File file = new File(outPath); // 新建一个pdf文档
            FileOutputStream os = new FileOutputStream(file);
            Document doc = new Document(inPath); // 验证License 是将要被转化的word文档

            System.out.println("开始解析word文档" + inPath);
            doc.save(os, SaveFormat.PDF);// 全面支持DOC, DOCX,
            // OOXML, RTF HTML,
            // OpenDocument,
            // PDF, EPUB, XPS,
            // SWF 相互转换
            long now = System.currentTimeMillis();//获取结束时间
            os.close();
            outMessage(inPath, outPath, now, old);
        } catch (Exception e) {
            System.out.println(inPath + "转换失败，请重试");
            e.printStackTrace();
        }
    }

    /**
     * 根据特定文字修改信息以及根据word文档中的书签添加图片
     *
     * @param srcPath
     * @param destPath
     * @param map
     */
    public static void searchAndReplace(String srcPath, String destPath, Map<String, String> map) {
        try {
            String str = "";
            for (int i = 1; i <= 5; i++) {
                if (i == 1) {
                    str = "生成1 -";
                } else {
                    str += "\r\t\t"+i+".生成 -"+i;
                }
            }
            //创建实例
            XWPFDocument document = new XWPFDocument(POIXMLDocument.openPackage(srcPath));
            //获取段落生成器
            Iterator<XWPFParagraph> itpara = document.getParagraphsIterator();
            while (itpara.hasNext()) {
                //获取到下一个
                XWPFParagraph next = itpara.next();
                Set<String> set = map.keySet();
                Iterator<String> iterator = set.iterator();
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    List<XWPFRun> runs = next.getRuns();
                    for (XWPFRun run : runs) {
                        String text = run.getText(0);
                        if (text != null && text.indexOf(key) != -1) {
                            text = text.replace(key, map.get(key));
                            System.out.println("key==" + key + "------runText==" + text);
                            run.setText(text, 0);
                        }
                        if (text != null && text.equals("**************")) {
                            System.out.println(run);
                            run.setText(str, 0);
                        }
                    }
                }
            }
            /**
             * 文档有表格把注释打开
             //获取表格迭代器
             Iterator<XWPFTable> itTable = document.getTablesIterator();
             while (itTable.hasNext()) {
             XWPFTable table = itTable.next();
             int rcount = table.getNumberOfRows();
             for (int i = 0; i < rcount; i++) {
             XWPFTableRow row = table.getRow(i);
             List<XWPFTableCell> cells = row.getTableCells();
             for (XWPFTableCell cell : cells) {
             for (Map.Entry<String, String> e : map.entrySet()) {
             if (cell.getText().equals(e.getKey())) {
             cell.removeParagraph(0);
             cell.setText(e.getValue());
             }
             }
             }
             }
             }
             */
            //获取到段落判断是否时有书签
            List<XWPFParagraph> paragraphs = document.getParagraphs();
            for (XWPFParagraph xwpfParagraph : paragraphs) {
                CTP ctp = xwpfParagraph.getCTP();

                for (int dwI = 0; dwI < ctp.sizeOfBookmarkStartArray(); dwI++) {
                    CTBookmark bookmark = ctp.getBookmarkStartArray(dwI);

                    String picIs = map.get(bookmark.getName());
                    System.out.println("-----------------------------" + picIs);
                    if (picIs != null) {
                        XWPFRun run = xwpfParagraph.createRun();
                        //bus.png为鼠标在word里选择图片时，图片显示的名字，400，400则为像素单元，根据实际需要的大小进行调整即可。
                        run.addPicture(new FileInputStream(picIs), XWPFDocument.PICTURE_TYPE_PNG, "page,", Units.toEMU(400), Units.toEMU(400));
                    }
                }
            }
            //修改完之后的文件
            FileOutputStream outputStream = new FileOutputStream(destPath);
            document.write(outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    public void paragraph(String docInputStream,String imgPath) throws Exception {
        Document doc = new Document(docInputStream);
        DocumentBuilder builder = new DocumentBuilder(doc);
        try{
            // 2.构建页面布局
            int pageCount = doc.getPageCount(); // 获取总页数
            LayoutCollector layoutCollector = new LayoutCollector(doc);
            doc.updatePageLayout();
            // 3.通过段落进行遍历（NodeType中包含各种类型可通过官网API对照）
            // https://apireference.aspose.com/words/java/com.aspose.words/NodeType
            NodeCollection runs = doc.getChildNodes(NodeType.PARAGRAPH, true);
            int tempPage = 0;
            for (int i = 0; i < runs.getCount(); i++) {
                Node r = runs.get(i);
                // 4.找到当前段落所对应的页数
                int numPage = layoutCollector.getStartPageIndex(r);
                // 5.进行图片插入
                if (numPage != tempPage) {
                    // 锁定到当前段落即实现页面变换
                    builder.moveTo(r);
                    // insertImage中参数含义自行查询
                    builder.insertImage(imgPath, RelativeHorizontalPosition.PAGE, 0, RelativeVerticalPosition.PAGE, 0, 100, 100, WrapType.NONE);
                }
                tempPage = numPage;
            }
            // 6.更新操作是将builder中修改数据更新到doc中（save方法中也会触发）
            doc.updateFields();
            //doc.save(outOs, saveOptions);
        } catch (Exception e) {}
    }

}
