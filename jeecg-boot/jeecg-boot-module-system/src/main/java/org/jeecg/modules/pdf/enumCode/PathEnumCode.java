package org.jeecg.modules.pdf.enumCode;

/**
 * 模板文件路径
 */
public enum PathEnumCode {

    //模板原件
    FILE_WHERE_PATH("C:\\Users\\admin\\Desktop\\原件.docx"),
    //修改后的word文档输出路径
    WORDFILE_OUT_PATH("C:\\Users\\admin\\Desktop\\改完后的文档.docx"),
    //word转换为PDF的路劲
    PDFFILE_OUT_PATH("C:\\Users\\admin\\Desktop\\改完后的文档.pdf");

    private String path;

    PathEnumCode(String paht) {
        this.path = paht;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
