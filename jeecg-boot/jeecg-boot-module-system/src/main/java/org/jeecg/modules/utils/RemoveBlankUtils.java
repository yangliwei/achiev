package org.jeecg.modules.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;


/**
 * 移除前后空格换行工具类
 * @Author: JiYiHuang
 * @Version: 1.1
 * @Date: 2022-07-14 09:22
 */
public class RemoveBlankUtils {

    /**
     * json字符串去除空格
     * public Object list(String params) {
     *      JSONObject jsonObject = RemoveBlankUtils.jsonRemoveBlank(params);
     * }
     * @param jsonString json字符串
     * @return
     */
    public static JSONObject jsonRemoveBlank(String jsonString) {
        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        return jsonRemoveBlank(jsonObject);
    }

    /**
     * json去除空格
     * public Object list(@RequestBody JSONObject params) {
     * 		 JSONObject jsonObject = RemoveBlankUtils.jsonRemoveBlank(params);
     * }
     * @param jsonObject json
     * @return
     */
    public static JSONObject jsonRemoveBlank(JSONObject jsonObject) {
        JSONObject data = new JSONObject();
        Set<String> keys = jsonObject.keySet();
        for (String key : keys) {
            // 得到string类型的value直接去除
            if (jsonObject.get(key) instanceof String) {
                String value = (String) jsonObject.get(key);
                data.put(key, StringUtils.isNotBlank(value) ? value.trim() : value);
            } else if (jsonObject.get(key) instanceof List) {
                // 得到集合类型则遍历后递归调用
                Object o = jsonObject.get(key);
                List<Object> list = (List<Object>) o;
                Object o1 = list.get(0);
                if (isJavaClass(o1.getClass())) {
                    List<Object> objects = changeList(o, Object.class);
                    if (objects != null) {
                        for (int i = 0; i < objects.size(); i++) {
                            if (isJavaClass(objects.get(i).getClass())) {
                                objects.set(i, objects.get(i).toString().trim());
                            } else {
                                Object o2 = entityRemoveBlank(objects.get(i), Object.class);
                                objects.set(i, o2);
                            }
                        }
                    }
                    data.put(key, objects);
                } else {
                    List<JSONObject> objects = changeList(o, JSONObject.class);
                    if (objects != null) {
                        for (int i = 0; i < objects.size(); i++) {
                            objects.set(i, jsonRemoveBlank(objects.get(i)));
                        }
                    }
                    data.put(key, objects);
                }
            } else {
                // 其他类型不做操作直接返回
                data.put(key, jsonObject.get(key));
            }
        }
        return data;
    }

    /**
     * 实体类去除空格
     * public Object list(User entity) {
     * 		 User u = RemoveBlankUtils.entityRemoveBlank(entity, User.class);
     * }
     * @param object 实体类
     * @param clazz 泛型类
     * @return
     */
    public static <T> T entityRemoveBlank(Object object, Class<T> clazz) {
        JSONObject jsonObject = JSON.parseObject(JSONObject.toJSONString(object));
        Set<String> keys = jsonObject.keySet();
        for (String key : keys) {
            Object value = jsonObject.get(key);
            // 如果值是字符串, 则直接处理
            if (value instanceof String) {
                jsonObject.put(key, StringUtils.isNotBlank((String) value) ? ((String) value).trim() : value);
            } else {
                boolean javaClass = isJavaClass(value.getClass());
                // 如果是Java原生的类, 可能为Integer, Long等..
                if (javaClass) {
                    jsonObject.put(key, StringUtils.isNotBlank(value.toString()) ? (value.toString()).trim() : value);

                // 如果是用户自定义的类
                } else {
                    // 如果是实体类集合
                    if (value instanceof List) {
                        value = listRemoveBlank(value, Object.class);
                        jsonObject.put(key, value);

                    // 如果是实体类
                    } else {
                        Object o = entityRemoveBlank(value, value.getClass());
                        jsonObject.put(key, o);
                    }
                }
            }
        }
        return JSONObject.toJavaObject(jsonObject, clazz);
    }

    /**
     * 对象类型的List集合处理
     * public Object list(List<User> list) {
     * 		 List<User> userList = RemoveBlankUtils.listRemoveBlank(list, User.class);
     * }
     * @param object 需要处理的对象
     * @param clazz 泛型类
     * @return
     */
    public static <T> List<T> listRemoveBlank(Object object, Class<T> clazz) {
        List<Object> list = new ArrayList<>();
        // 取到集合内的每个对象依次处理
        List<?> objects = changeList(object, clazz);
        if (objects != null&&objects.size()>0) {
            // 得到集合的类型
            Object o1 = objects.get(0);
            boolean javaClass = isJavaClass(o1.getClass());
            for (Object o :  objects) {
                // 如果是java自带的类, 如string, Integer等..
                if (javaClass) {
                    String trim = o.toString().trim();
                    list.add(trim);
                // 如果是用户自定义的实体类
                } else {
                    Object data = entityRemoveBlank(o, clazz);
                    list.add(data);
                }
            }
            return changeList(list, clazz);
        }
        return null;
    }

    /**
     * 对象类型的Set集合处理
     * public Object list(Set<User> list) {
     * 		 Set<User> userList = RemoveBlankUtils.setRemoveBlank(list, User.class);
     * }
     * @param object 需要处理的对象
     * @param clazz 泛型类
     * @return
     */
    public static <T> Set<T> setRemoveBlank(Object object, Class<T> clazz) {
        Set<Object> set = new HashSet<>();
        Set<?> objects = changeSet(object, clazz);
        if (objects != null) {
            // 得到集合的类型
            for (Object o :  objects) {
                boolean javaClass = isJavaClass(o.getClass());
                // 如果是java自带的类, 如string, Integer等..
                if (javaClass) {
                    String trim = o.toString().trim();
                    set.add(trim);
                // 如果是用户自定义的实体类
                } else {
                    Object data = entityRemoveBlank(o, clazz);
                    set.add(data);
                }
            }
            return changeSet(set, clazz);
        }
        return null;
    }

    /**
     * Map处理
     * public Object list(Map<String, String> map) {
     * 		 RemoveBlankUtils.mapRemoveBlank(map);
     * }
     * @param map 需要处理的Map
     */
    public static void mapRemoveBlank(Map<String, String> map) {
        Set<String> objects = map.keySet();
        for (String key : objects) {
            String value = map.get(key);
            map.put(key, StringUtils.isNotBlank(value) ? value.trim() : value);
        }
    }

    /**
     * Object对象转 List集合
     * @param object 需要转换的对象
     * @param clazz 泛型类
     * @return
     */
    private static <T> List<T> changeList(Object object,Class<T> clazz){
        try {
            List<T> result = new ArrayList<>();
            if (object instanceof List<?>){
                for (Object o : (List<?>) object) {
                    String string = JSONObject.toJSONString(o);
                    T t = JSONObject.parseObject(string, clazz);
                    result.add(t);
                }
                return result;
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Object对象转 Set集合
     * @param object 需要转换的对象
     * @param clazz 泛型类
     * @return
     */
    private static <T> Set<T> changeSet(Object object,Class<T> clazz){
        try {
            Set<T> result = new HashSet<>();
            if (object instanceof Set<?>){
                for (Object o : (Set<?>) object) {
                    String string = JSONObject.toJSONString(o);
                    T t = JSONObject.parseObject(string, clazz);
                    result.add(t);
                }
                return result;
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 判断类是Java类型还是自定义类型
     * @param clazz 需要判断的类
     * @return
     */
    private static boolean isJavaClass(Class<?> clazz) {
        return clazz != null && clazz.getClassLoader() == null;
    }

    /**
     * 将map里的值拷贝到对应实体类中
     * @param map
     * @param clazz
     * @return
     */
    private static Object mapTobject(Map<?, ?> map,Class<?> clazz){
        if(map == null){
            return null;
        }
        Object obj = null;
        try {
            obj = clazz.newInstance();
            //获取到所有属性，不包括继承的属性
            Field[] fields = obj.getClass().getDeclaredFields();
            //获取传入类的父类的所有属性
            Field[] supFields = obj.getClass().getSuperclass().getDeclaredFields();
            for(Field field : fields){
                //获取字段的修饰符
                int mod = field.getModifiers();
                if(Modifier.isStatic(mod) || Modifier.isFinal(mod)){
                    continue;
                }
                field.setAccessible(true);
                //根据属性名称去map获取value
                field.set(obj, map.get(field.getName()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

}
