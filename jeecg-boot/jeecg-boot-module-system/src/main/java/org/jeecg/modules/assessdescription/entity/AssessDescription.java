package org.jeecg.modules.assessdescription.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 考评描述表
 * @Author: jeecg-boot
 * @Date:   2023-02-14
 * @Version: V1.0
 */
@Data
@TableName("assess_description")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="assess_description对象", description="考评描述表")
public class AssessDescription implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**方案描述id*/
	@Excel(name = "方案描述id", width = 15)
    @ApiModelProperty(value = "方案描述id")
    private String schemeDescriptionId;
	/**考评内容id*/
	@Excel(name = "考评内容id", width = 15)
    @ApiModelProperty(value = "考评内容id")
    private String assessContentId;
	/**考评id*/
	@Excel(name = "考评id", width = 15)
    @ApiModelProperty(value = "考评id")
    private String assessId;
	/**需完成数*/
	@Excel(name = "需完成数", width = 15)
    @ApiModelProperty(value = "需完成数")
    private Double contentShouldCompleted;
	/**实际完成数*/
	@Excel(name = "实际完成数", width = 15)
    @ApiModelProperty(value = "实际完成数")
    private Double contentActualCompleted;
	/**完成率*/
	@Excel(name = "完成率", width = 15)
    @ApiModelProperty(value = "完成率")
    private Double contentCompletionRate;
}
