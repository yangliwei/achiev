package org.jeecg.modules.message.handle.impl;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.formula.functions.T;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.config.StaticConfig;
import org.jeecg.modules.message.handle.ISendMsgHandle;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Properties;

public class EmailSendMsgHandle implements ISendMsgHandle {
    private static String emailFrom;

    private final static String hosts = "smtp.139.com,smtp.139.com";
    private final static String usernames = "azxsh_PingAn@139.com,azxsh_RenBao@139.com";
    private final static String passwords = "f8b8da46cd17809e8d00,f6a679cb46f9bbe66700";

    public static void setEmailFrom(String emailFrom) {
        EmailSendMsgHandle.emailFrom = emailFrom;
    }

    @Override
    public void SendMsg(String es_receiver, String es_title, String es_content) {
        JavaMailSender mailSender = (JavaMailSender) SpringContextUtils.getBean("mailSender");
        MimeMessage message = mailSender.createMimeMessage();
        try {
            message.addRecipients(MimeMessage.RecipientType.CC, InternetAddress.parse("azxsh_RenBao@139.com"));
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        MimeMessageHelper helper = null;
        //update-begin-author：taoyan date:20200811 for:配置类数据获取
        if (oConvertUtils.isEmpty(emailFrom)) {
            StaticConfig staticConfig = SpringContextUtils.getBean(StaticConfig.class);
            setEmailFrom(staticConfig.getEmailFrom());
        }
        //update-end-author：taoyan date:20200811 for:配置类数据获取
        try {
            helper = new MimeMessageHelper(message, true);
            // 设置发送方邮箱地址
            helper.setFrom(emailFrom);
            helper.setTo(es_receiver);
            helper.setSubject(es_title);
            helper.setText(es_content, true);
            mailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    /**
     * 邮件发送（附带邮件）
     *
     * @param es_receiver
     * @param es_title
     * @param es_content
     * @param inputStream
     * @param belong
     */
    public void SendMsg(String es_receiver, String es_title, String es_content, InputStream inputStream, String belong) {
        if (oConvertUtils.isEmpty(emailFrom)) {
            setEmailFrom(belong.equals("001") ? usernames.split(",")[0] : usernames.split(",")[1]);
        }

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        //配置邮箱信息
        mailSender.setPort(465);
        mailSender.setHost(belong.equals("001") ? hosts.split(",")[0] : hosts.split(",")[1]);
        mailSender.setUsername(belong.equals("001") ? usernames.split(",")[0] : usernames.split(",")[1]);
        mailSender.setPassword(belong.equals("001") ? passwords.split(",")[0] : passwords.split(",")[1]);

        //设置发送邮件参数
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.transport.protocol", "smtp");
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        mailSender.setJavaMailProperties(properties);

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        //update-end-author：taoyan date:20200811 for:配置类数据获取
        try {
            helper = new MimeMessageHelper(message, true, "UTF-8");

            // 设置发送方邮箱地址
            helper.setFrom(belong.equals("001") ? usernames.split(",")[0] : usernames.split(",")[1]);
            //底下这几个是我传的参，它总共获取让我操作的就只有emailFrom是设置发件人的邮件
            helper.setTo(es_receiver);
            helper.setSubject(es_title);
            helper.setText(es_content, true);
            ByteArrayResource byteArrayResource = new ByteArrayResource(IOUtils.toByteArray(inputStream));
            helper.addAttachment("排查服务记录报告.docx", byteArrayResource);
            mailSender.send(message);
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }

    }

}
