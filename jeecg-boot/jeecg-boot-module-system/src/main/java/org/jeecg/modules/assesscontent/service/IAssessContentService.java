package org.jeecg.modules.assesscontent.service;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.assesscontent.entity.AssessContent;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentGroup;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentVo;

import java.util.List;

/**
 * @Description: 考评内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface IAssessContentService extends IService<AssessContent> {

    /**
     * 查询绩效考评内容_根据考评id
     * @param id
     * @param schemeId
     * @return
     */
    List<AssessContentVo> queryAssessContentVoByAssessId(String id, String schemeId);

    /**
     * 查询绩效考评内容, 只查询自己负责的数据_根据考评id和用户id
     * @param id
     * @param schemeId
     * @param userId
     * @return
     */
    List<AssessContentVo> queryAssessContentVoByAssessId(String id, String schemeId, String userId);

    /**
     * 修改绩效考评内容的平均分
     * @param id
     */
    void updateAssessContentAvg(String id);

    List<AssessContentGroup> AssessContentListGroup(List<AssessContentVo> assessContentVoList);

    void updateBatchAssessContentAvg(@Param("list") List<String> list);

    /**
     * 查询考评内容无评分数据的数据
     * @return
     */
    List<AssessContent> queryWithoutScoreDataList();


}
