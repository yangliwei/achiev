package org.jeecg.modules.currencytemplate.service.impl;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.PasswordUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.currencytemplate.entity.TemplateCurrency;
import org.jeecg.modules.currencytemplate.mapper.TemplateCurrencyMapper;
import org.jeecg.modules.currencytemplate.service.ITemplateCurrencyService;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: 模板通用项
 * @Author: jeecg-boot
 * @Date:   2021-05-27
 * @Version: V1.0
 */
@Service
public class TemplateCurrencyServiceImpl extends ServiceImpl<TemplateCurrencyMapper, TemplateCurrency> implements ITemplateCurrencyService {

    @Autowired
    TemplateCurrencyMapper templateCurrencyMapper;

    @Override
    public Result<?> importCurrency(HttpServletRequest request, HttpServletResponse response, Class<TemplateCurrency> templateCurrencyClass) {

        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();// 获取上传文件对象
            ImportParams params = new ImportParams();
            // 表格标题所在行，计数从0开始ff
            params.setTitleRows(0);
            // head头部所在行，计数从0开始
            params.setHeadRows(0);
            params.setNeedSave(true);
            long start = System.currentTimeMillis();
            try {
                List<TemplateCurrency> list = ExcelImportUtil.importExcel(file.getInputStream(), templateCurrencyClass, params);
                this.saveBatch(list);
                return Result.ok("文件导入成功：新增"+list.size()+"条数据");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            } finally {
                try {
                    file.getInputStream().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.error("文件导入失败！");
    }
}
