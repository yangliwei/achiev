package org.jeecg.modules.currencytemplate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.currencytemplate.entity.TemplateCurrency;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 模板通用项
 * @Author: jeecg-boot
 * @Date:   2021-05-27
 * @Version: V1.0
 */
public interface ITemplateCurrencyService extends IService<TemplateCurrency> {

    /**
     * 导入通用项
     * @param request
     * @param response
     * @param templateCurrencyClass
     * @return
     */
    public Result<?> importCurrency(HttpServletRequest request, HttpServletResponse response, Class<TemplateCurrency> templateCurrencyClass);
}
