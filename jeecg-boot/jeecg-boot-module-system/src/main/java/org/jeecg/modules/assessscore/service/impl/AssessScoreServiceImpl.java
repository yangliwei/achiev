package org.jeecg.modules.assessscore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.achievassess.service.IAchievAssessService;
import org.jeecg.modules.assesscontent.service.IAssessContentService;
import org.jeecg.modules.assessscore.entity.AssessScore;
import org.jeecg.modules.assessscore.mapper.AssessScoreMapper;
import org.jeecg.modules.assessscore.service.IAssessScoreService;
import org.jeecg.modules.schemecontent.service.ISchemeContentService;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecg.modules.system.service.ISysUserDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 评分表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Slf4j
@Service
public class AssessScoreServiceImpl extends ServiceImpl<AssessScoreMapper, AssessScore> implements IAssessScoreService {

    @Autowired
    private ISysDepartService sysDepartService;
    @Autowired
    private ISysUserDepartService sysUserDepartService;
    // 懒加载, 解决循环依赖问题
    @Lazy
    @Autowired
    private IAchievAssessService achievAssessService;
    @Autowired
    private ISchemeContentService schemeContentService;

    @Override
    public void generateAssessScore(String achievAssessId, String assessContentId, String schemeAppraiserId, String schemeAppraiserName, Map<String, List<AssessScore>> assessScoreMap, String assessAppraisee) {
        // 如果受评人和评分人是同一个, 则先不生成, 如果考评内容只有ta一个人的时候再在后面不查询追加
        if (assessAppraisee.equals(schemeAppraiserId)) {
            return;
        }
        AssessScore assessScore = new AssessScore();
        assessScore.setAssessId(achievAssessId);
        assessScore.setAssessContentId(assessContentId);
        assessScore.setScoreAppraiser(schemeAppraiserId);
        assessScore.setScoreAppraiserName(schemeAppraiserName);
        List<AssessScore> assessScoreList = assessScoreMap.get(assessContentId);
        if (ObjectUtils.isEmpty(assessScoreList)) {
            this.save(assessScore);
            List<AssessScore> assessScoreMapList = new ArrayList<>();
            assessScoreMapList.add(assessScore);
            assessScoreMap.put(assessContentId, assessScoreMapList);
        } else {
            // 如果本月内已经存在该考评id,考评内容id和考评人id的数据, 则不进行添加
            boolean parallel = assessScoreList.stream().anyMatch(item -> achievAssessId.equals(item.getAssessId()) && assessContentId.equals(item.getAssessContentId()) && StringUtils.equals(schemeAppraiserId, item.getScoreAppraiser()));
            if (!parallel) {
                this.save(assessScore);
                List<AssessScore> assessScoreMapList = assessScoreMap.get(assessContentId);
                assessScoreMapList.add(assessScore);
                assessScoreMap.put(assessContentId, assessScoreMapList);
            }
        }
    }

    // 特殊考评人
    @Override
    public List<SysUser> querySpecialAppraiser(String userId, String schemeAppraiserName, String departId) {
        SysDepart sysDepart = sysDepartService.getById(departId);
        switch (schemeAppraiserName) {
            case "上级领导":
                String parentId = sysDepart.getParentId();
                List<SysUser> sysUserList = sysUserDepartService.queryUserByDepId(parentId);
                if (ObjectUtils.isNotEmpty(sysUserList)) {
                    return sysUserList;
                }
                break;
            case "本部门其他人员":
                List<SysUser> sysUsers = sysUserDepartService.queryUserByDepId(departId);
                return sysUsers.stream().filter(item -> !userId.equals(item.getId())).collect(Collectors.toList());
            case "部门成员":
                List<SysDepart> sysDepartList = sysDepartService.queryDeptByPid(departId);
                List<SysUser> resultData = new ArrayList<>();
                for (SysDepart depart : sysDepartList) {
                    List<SysUser> userList = sysUserDepartService.queryUserByDepId(depart.getId());
                    resultData.addAll(userList);
                }
                return resultData;
            default:
                break;
        }
        return new ArrayList<>();
    }

    @Override
    public void modifyUnappraisal() {
        List<String> assessIdList1 = baseMapper.queryModifyUnappraisalAssessIdListSingle();
        baseMapper.modifyUnappraisalSingle();
        achievAssessService.updateBatchAssessContentAvg(assessIdList1);
        log.info("更新未评的单人评分数据完成---");
        List<String> assessIdList2 = baseMapper.queryModifyUnappraisalAssessIdListMany();
        baseMapper.modifyUnappraisalMany();
        achievAssessService.updateBatchAssessContentAvg(assessIdList2);
        log.info("更新完全未评的多人评分数据完成---");
    }
}
