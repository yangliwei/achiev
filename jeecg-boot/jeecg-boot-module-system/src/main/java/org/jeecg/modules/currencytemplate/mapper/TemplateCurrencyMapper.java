package org.jeecg.modules.currencytemplate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.currencytemplate.entity.TemplateCurrency;

/**
 * @Description: 模板通用项
 * @Author: jeecg-boot
 * @Date:   2021-05-27
 * @Version: V1.0
 */
public interface TemplateCurrencyMapper extends BaseMapper<TemplateCurrency> {

}
