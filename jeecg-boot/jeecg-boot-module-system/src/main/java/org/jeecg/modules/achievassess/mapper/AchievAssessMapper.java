package org.jeecg.modules.achievassess.mapper;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.achievassess.entity.AchievAssess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessBo;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessVo;

import javax.annotation.Resource;

/**
 * @Description: 绩效考评总表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */

public interface AchievAssessMapper extends BaseMapper<AchievAssess> {

     List<AchievAssessBo> queryAssessAppraiseeNameAndScoringCount(@Param("date") String date);

    IPage<AchievAssessVo> queryResponsibleData(@Param("page") Page<AchievAssessVo> page, @Param("userId") String userId, @Param("userName") String userName, @Param("schemeName") String schemeName, @Param("date") String date);

    IPage<AchievAssessVo> querySchemeByAppraisee(@Param("page") Page<AchievAssessVo> page, @Param("userId") String userId, @Param("date") String date, @Param("userName") String userName, @Param("schemeName") String schemeName);

    IPage<AchievAssessVo> queryCompletionRate(@Param("page") Page<AchievAssessVo> page, @Param("userId") String userId, @Param("date") String date, @Param("name") String name);

    AchievAssessVo queryCompletionRateInfo(@Param("id") String id);

    boolean queryAssessIsLast(@Param("id") String id);

    void updateBatchAssessTotal(@Param("list") List<String> list);

}
