package org.jeecg.modules.schemeappraiser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 方案考评人表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface SchemeAppraiserMapper extends BaseMapper<SchemeAppraiser> {

    List<SchemeAppraiser> queryNotDeletedSchemeAppraiser();

}
