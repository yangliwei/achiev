package org.jeecg.modules.schemedescription.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import org.jeecg.modules.schemedescription.service.ISchemeDescriptionService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 程度描述表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Api(tags="程度描述表")
@RestController
@RequestMapping("/schemedescription/schemeDescription")
@Slf4j
public class SchemeDescriptionController extends JeecgController<SchemeDescription, ISchemeDescriptionService> {
	@Autowired
	private ISchemeDescriptionService schemeDescriptionService;
	
	/**
	 * 分页列表查询
	 *
	 * @param schemeDescription
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "程度描述表-分页列表查询")
	@ApiOperation(value="程度描述表-分页列表查询", notes="程度描述表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SchemeDescription schemeDescription,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SchemeDescription> queryWrapper = QueryGenerator.initQueryWrapper(schemeDescription, req.getParameterMap());
		Page<SchemeDescription> page = new Page<SchemeDescription>(pageNo, pageSize);
		IPage<SchemeDescription> pageList = schemeDescriptionService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param schemeDescription
	 * @return
	 */
	@AutoLog(value = "程度描述表-添加")
	@ApiOperation(value="程度描述表-添加", notes="程度描述表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SchemeDescription schemeDescription) {
		schemeDescriptionService.save(schemeDescription);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param schemeDescription
	 * @return
	 */
	@AutoLog(value = "程度描述表-编辑")
	@ApiOperation(value="程度描述表-编辑", notes="程度描述表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SchemeDescription schemeDescription) {
		schemeDescriptionService.updateById(schemeDescription);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "程度描述表-通过id删除")
	@ApiOperation(value="程度描述表-通过id删除", notes="程度描述表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		schemeDescriptionService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "程度描述表-批量删除")
	@ApiOperation(value="程度描述表-批量删除", notes="程度描述表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.schemeDescriptionService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "程度描述表-通过id查询")
	@ApiOperation(value="程度描述表-通过id查询", notes="程度描述表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SchemeDescription schemeDescription = schemeDescriptionService.getById(id);
		if(schemeDescription==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(schemeDescription);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param schemeDescription
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SchemeDescription schemeDescription) {
        return super.exportXls(request, schemeDescription, SchemeDescription.class, "程度描述表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, SchemeDescription.class);
    }

}
