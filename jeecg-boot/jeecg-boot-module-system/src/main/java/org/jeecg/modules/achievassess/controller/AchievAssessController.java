package org.jeecg.modules.achievassess.controller;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.checkerframework.checker.units.qual.A;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.achievassess.entity.AchievAssess;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessBo;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessVo;
import org.jeecg.modules.achievassess.service.IAchievAssessService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.achievscheme.entity.vo.AchievSchemeVo;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentGroup;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentVo;
import org.jeecg.modules.assessscore.entity.AssessScore;
import org.jeecg.modules.assessscore.service.IAssessScoreService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 绩效考评总表
 * @Author: jeecg-boot
 * @Date: 2023-01-16
 * @Version: V1.0
 */
@Api(tags = "绩效考评总表")
@RestController
@RequestMapping("/achievassess/achievAssess")
@Slf4j
public class AchievAssessController extends JeecgController<AchievAssess, IAchievAssessService> {
    @Autowired
    private IAchievAssessService achievAssessService;
    @Autowired
    private IAssessScoreService assessScoreService;

    /**
     * 分页列表查询
     *
     * @param achievAssess
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "绩效考评总表-分页列表查询")
    @ApiOperation(value = "绩效考评总表-分页列表查询", notes = "绩效考评总表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(AchievAssess achievAssess,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<AchievAssess> queryWrapper = QueryGenerator.initQueryWrapper(achievAssess, req.getParameterMap());
        Page<AchievAssess> page = new Page<AchievAssess>(pageNo, pageSize);
        IPage<AchievAssess> pageList = achievAssessService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param achievAssess
     * @return
     */
    @AutoLog(value = "绩效考评总表-添加")
    @ApiOperation(value = "绩效考评总表-添加", notes = "绩效考评总表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody AchievAssess achievAssess) {
        achievAssessService.save(achievAssess);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param achievAssess
     * @return
     */
    @AutoLog(value = "绩效考评总表-编辑")
    @ApiOperation(value = "绩效考评总表-编辑", notes = "绩效考评总表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody AchievAssess achievAssess) {
        achievAssessService.updateById(achievAssess);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绩效考评总表-通过id删除")
    @ApiOperation(value = "绩效考评总表-通过id删除", notes = "绩效考评总表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        achievAssessService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "绩效考评总表-批量删除")
    @ApiOperation(value = "绩效考评总表-批量删除", notes = "绩效考评总表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.achievAssessService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绩效考评总表-通过id查询")
    @ApiOperation(value = "绩效考评总表-通过id查询", notes = "绩效考评总表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        AchievAssess achievAssess = achievAssessService.getById(id);
        if (achievAssess == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(achievAssess);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param achievAssess
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, AchievAssess achievAssess) {
        return super.exportXls(request, achievAssess, AchievAssess.class, "绩效考评总表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, AchievAssess.class);
    }

    /**
     * 生成考评表
     *
     * @return
     */
    @AutoLog(value = "生成考评表")
    @ApiOperation(value = "生成考评表", notes = "生成考评表")
    @GetMapping(value = "/generatePerformanceAppraisal")
    public Result<?> generatePerformanceAppraisal() {
        assessScoreService.modifyUnappraisal();
        achievAssessService.generatePerformanceAppraisal();
        return Result.OK("生成完成!");
    }

    /**
     * 查询指定月份的全部数据
     *
     * @param date
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "查询指定月份的全部数据")
    @ApiOperation(value = "查询指定月份的全部数据", notes = "查询全部指定月份的全部数据")
    @GetMapping(value = "/queryAllScheme")
    public Result<?> queryAllScheme(@RequestParam(name = "date", required = false) String date,
                                    @RequestParam(name = "userName", required = false) String userName,
                                    @RequestParam(name = "schemeName", required = false) String schemeName,
                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        if (StringUtils.isBlank(date) && (StringUtils.isBlank(userName) && StringUtils.isBlank(schemeName))) {
            return Result.error("查询失败, 不指定日期时用户名和方案名不能同时为空");
        }
        Page<AchievAssessVo> page = new Page<AchievAssessVo>(pageNo, pageSize);
        IPage<AchievAssessVo> pageList = achievAssessService.querySchemeByAppraisee(page, null, date, userName, schemeName);
        return Result.OK(pageList);
    }

    /**
     * 查询考评表信息_根据id
     *
     * @param id 考评表id
     * @return
     */
    @AutoLog(value = "查询考评表信息_根据id")
    @ApiOperation(value = "查询考评表信息_根据id", notes = "查询考评表信息_根据id")
    @GetMapping("/queryAssessById")
    public Result<?> queryAssessById(@RequestParam("id") String id) {
        AchievAssessVo achievAssessVo = achievAssessService.queryAssessById(id);
        return Result.OK(achievAssessVo);
    }

    /**
     * 查询自己的考评数据
     *
     * @param userId
     * @param date
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "查询自己的考评数据")
    @ApiOperation(value = "查询自己的考评数据", notes = "查询自己的考评数据")
    @GetMapping(value = "/querySchemeByAppraisee")
    public Result<?> querySchemeByAppraisee(@RequestParam(name = "userId") String userId,
                                            @RequestParam(name = "date", required = false) String date,
                                            @RequestParam(name = "userName", required = false) String userName,
                                            @RequestParam(name = "schemeName", required = false) String schemeName,
                                            @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<AchievAssessVo> page = new Page<AchievAssessVo>(pageNo, pageSize);
        IPage<AchievAssessVo> pageList = achievAssessService.querySchemeByAppraisee(page, userId, date, userName, schemeName);
        return Result.OK(pageList);
    }

    /**
     * 查询自己考评数据的详细
     * @param id
     * @return
     */
    @AutoLog(value = "查询自己考评数据的详细")
    @ApiOperation(value = "查询自己考评数据的详细", notes = "查询自己的绩效数据")
    @GetMapping(value = "/queryOwnSchemeInfo")
    public Result<?> queryOwnSchemeInfo(@RequestParam("id") String id) {
        AchievAssessVo achievAssessVo = achievAssessService.queryAssessById(id);
        List<AssessContentGroup> assessContentGroup = achievAssessVo.getAssessContentGroup();
        for (AssessContentGroup contentGroup : assessContentGroup) {
            for (AssessContentVo assessContentVo : contentGroup.getAssessContentVoList()) {
                for (AssessScore assessScore : assessContentVo.getAssessScoreList()) {
                    assessScore.setScoreTotal(null);
                }
            }
        }
        return Result.OK(achievAssessVo);
    }

    /**
     * 分页列表查询_查询当前用户所负责的评分的数据
     *
     * @param userId
     * @param date
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "分页列表查询_查询当前用户所负责的评分的数据")
    @ApiOperation(value = "分页列表查询_查询当前用户所负责的评分的数据", notes = "分页列表查询_查询当前用户所负责的评分的数据")
    @GetMapping(value = "/queryResponsibleData")
    public Result<?> queryResponsibleData(@RequestParam(name = "userId") String userId,
                                          @RequestParam(name = "date", required = false) String date,
                                          @RequestParam(name = "userName", required = false) String userName,
                                          @RequestParam(name = "schemeName", required = false) String schemeName,
                                          @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                          @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<AchievAssessVo> page = new Page<AchievAssessVo>(pageNo, pageSize);
        IPage<AchievAssessVo> pageList = achievAssessService.queryResponsibleData(page, userId, userName, schemeName, date);
        return Result.OK(pageList);
    }

    /**
     * 查询考评表信息_根据用户查该考评表内用户负责的考评数据
     *
     * @param id     考评表id
     * @param userId 用户id
     * @return
     */
    @AutoLog(value = "查询考评表信息_根据用户查该考评表内用户负责的考评数据")
    @ApiOperation(value = "查询考评表信息_根据用户查该考评表内用户负责的考评数据", notes = "查询考评表信息_根据用户查该考评表内用户负责的考评数据")
    @GetMapping("/queryAssessByIdAndUserId")
    public Result<?> queryAssessByIdAndUserId(@RequestParam("id") String id, @RequestParam("userId") String userId) {
        AchievAssessVo achievAssessVo = achievAssessService.queryAssessByIdAndUserId(id, userId);
        return Result.OK(achievAssessVo);
    }

    /**
     * 修改评分
     * @param achievAssessVo
     * @return
     */
    @AutoLog(value = "修改评分")
    @ApiOperation(value = "修改评分", notes = "修改评分")
    @PutMapping("/updateAssessScore")
    public Result<?> updateAssessScore(@RequestBody AchievAssessVo achievAssessVo) throws ParseException {
        achievAssessService.updateAssessScore(achievAssessVo);
        achievAssessService.updateAssessContentAvg(achievAssessVo.getId());
        return Result.OK();
    }

    /**
     * 查询完成率评分数据
     *
     * @param date
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "查询完成率评分数据")
    @ApiOperation(value = "查询完成率评分数据", notes = "查询完成率评分数据")
    @GetMapping(value = "/queryCompletionRate")
    public Result<?> queryCompletionRate(@RequestParam(name = "userId") String userId,
                                         @RequestParam(name = "date") String date,
                                         @RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                         @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<AchievAssessVo> page = new Page<AchievAssessVo>(pageNo, pageSize);
        IPage<AchievAssessVo> pageList = achievAssessService.queryCompletionRate(page, userId, date, name);
        return Result.OK(pageList);
    }

    /**
     * 查询完成率评分数据详细信息
     *
     * @param id
     * @return
     */
    @AutoLog(value = "查询完成率评分数据详细信息")
    @ApiOperation(value = "查询完成率评分数据详细信息", notes = "查询完成率评分数据详细信息")
    @GetMapping("/queryCompletionRateInfo")
    public Result<?> queryCompletionRateInfo(@RequestParam("id") String id) {
        AchievAssessVo achievAssessVo = achievAssessService.queryCompletionRateInfo(id);
        return Result.OK(achievAssessVo);
    }

    /**
     * 修改完成率
     * @param achievAssess
     * @return
     */
    @AutoLog(value = "修改完成率")
    @ApiOperation(value = "修改完成率", notes = "修改完成率")
    @PutMapping("/updateCompletionRate")
    public Result<?> updateCompletionRate(@RequestBody AchievAssess achievAssess) throws ParseException {
        achievAssessService.updateCompletionRate(achievAssess);
        return Result.OK();
    }

    /**
     * 是否结束考评
     * @return
     */
    @AutoLog(value = "是否结束考评")
    @ApiOperation(value = "是否结束考评", notes = "是否结束考评")
    @GetMapping("/isFinishAssess")
    public Result<?> isFinishAssess() {
        boolean flag = achievAssessService.isFinishAssess();
        return Result.OK(flag);
    }

    /**
     * 查询最近一次数据的月份
     * @return
     */
    @AutoLog(value = "查询最近一次数据的月份")
    @ApiOperation(value = "查询最近一次数据的月份", notes = "查询最近一次数据的月份")
    @GetMapping("/queryLastDataMonth")
    public Result<?> queryLastDataMonth() {
        String data = achievAssessService.queryLastDataMonth();
        return Result.OK(data);
    }


    /**
     * 下载考评系统的Excel
     * @param date
     * @return
     */
    @AutoLog(value = "下载考评系统的Excel")
    @ApiOperation(value = "下载考评系统的Excel", notes = "下载考评系统的Excel")
    @GetMapping("/downloadExcpelSource")

    public ModelAndView downloadExcpelSource(HttpServletRequest request, @RequestParam("date") String date) {

        List<AchievAssessBo> achievAssessBoList = achievAssessService.getAssessAppraiseeNameAndScoringCount(date);
      String year = date.substring(0, 4);
      String month = date.substring(5,7);
        // Step.3 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, year+"年"+month+"月"+"绩效考评成绩");
        mv.addObject(NormalExcelConstants.CLASS, AchievAssessBo.class);
       ExportParams  exportParams=new ExportParams( year+"年"+month+"月"+"绩效考评成绩", year+"年"+month+"月"+"绩效考评成绩");
        mv.addObject(NormalExcelConstants.PARAMS,exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, achievAssessBoList);
        return mv;
    }

}
