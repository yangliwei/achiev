package org.jeecg.modules.achievscheme.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.achievscheme.entity.AchievScheme;
import org.jeecg.modules.achievscheme.entity.vo.AchievSchemeVo;
import org.jeecg.modules.achievscheme.service.IAchievSchemeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.schemecontent.entity.vo.SchemeContentVo;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 考评方案总案
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Api(tags="考评方案总案")
@RestController
@RequestMapping("/achievscheme/achievScheme")
@Slf4j
public class AchievSchemeController extends JeecgController<AchievScheme, IAchievSchemeService> {
	@Autowired
	private IAchievSchemeService achievSchemeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param achievScheme
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "考评方案总案-分页列表查询")
	@ApiOperation(value="考评方案总案-分页列表查询", notes="考评方案总案-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(AchievScheme achievScheme,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<AchievScheme> queryWrapper = QueryGenerator.initQueryWrapper(achievScheme, req.getParameterMap());
		Page<AchievScheme> page = new Page<AchievScheme>(pageNo, pageSize);
		IPage<AchievScheme> pageList = achievSchemeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param achievScheme
	 * @return
	 */
	@AutoLog(value = "考评方案总案-添加")
	@ApiOperation(value="考评方案总案-添加", notes="考评方案总案-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody AchievScheme achievScheme) {
		achievSchemeService.save(achievScheme);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param achievScheme
	 * @return
	 */
	@AutoLog(value = "考评方案总案-编辑")
	@ApiOperation(value="考评方案总案-编辑", notes="考评方案总案-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody AchievScheme achievScheme) {
		achievSchemeService.updateById(achievScheme);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "考评方案总案-通过id删除")
	@ApiOperation(value="考评方案总案-通过id删除", notes="考评方案总案-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		achievSchemeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "考评方案总案-批量删除")
	@ApiOperation(value="考评方案总案-批量删除", notes="考评方案总案-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.achievSchemeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "考评方案总案-通过id查询")
	@ApiOperation(value="考评方案总案-通过id查询", notes="考评方案总案-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		AchievScheme achievScheme = achievSchemeService.getById(id);
		if(achievScheme==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(achievScheme);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param achievScheme
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, AchievScheme achievScheme) {
        return super.exportXls(request, achievScheme, AchievScheme.class, "考评方案总案");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, AchievScheme.class);
    }


	 /**
	  * 查询方案表信息_根据id
	  * @param id 方案总表id
	  * @return
	  */
	 @AutoLog(value = "查询方案表信息_根据id")
	 @ApiOperation(value="查询方案表信息_根据id", notes="查询方案表信息_根据id")
	 @GetMapping("/querySchemeById")
	 public Result<?> querySchemeById(@RequestParam("id") String id) {
		 AchievSchemeVo achievSchemeVo = achievSchemeService.querySchemeById(id);
		 return Result.OK(achievSchemeVo);
	 }

	 /**
	  * 新增或修改考评表模板
	  * @param achievSchemeVo 数据
	  * @return
	  */
	 @AutoLog(value = "新增或修改考评表模板")
	 @ApiOperation(value="新增或修改考评表模板", notes="新增或修改考评表模板")
	 @PostMapping("/saveOrUpdateScheme")
	 public Result<?> saveOrUpdateScheme(@RequestBody AchievSchemeVo achievSchemeVo) {
		 for (SchemeContentVo schemeContentVo : achievSchemeVo.getSchemeContentList()) {
			 Pattern compile = Pattern.compile("(?<!（\\d{1,2}）)$", Pattern.DOTALL);
			 Matcher matcher = compile.matcher(schemeContentVo.getSchemeContent());
			 if (matcher.find()) {
				 return Result.error("考核内容: " + schemeContentVo.getSchemeContent() + ", 没有以（分值）结尾");
			 }
		 }
		 achievSchemeService.saveOrUpdateScheme(achievSchemeVo);
		 return Result.OK();
	 }

	 /**
	  * 删除考评表模板_软删除
	  * @param id 考评总表id
	  * @return
	  */
	 @AutoLog(value = "删除考评表模板_软删除")
	 @ApiOperation(value="删除考评表模板_软删除", notes="删除考评表模板_软删除")
	 @DeleteMapping("/deleteScheme")
	 public Result<?> deleteScheme(@RequestParam("id") String id) {
		 achievSchemeService.removeById(id);
		 return Result.OK();
	 }

}
