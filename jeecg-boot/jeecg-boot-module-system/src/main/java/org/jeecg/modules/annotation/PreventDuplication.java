package org.jeecg.modules.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PreventDuplication {
    /**
     * 防重复操作限时标记数值
     */
    String value() default "value" ;

    /**
     * 防重复操作过期时间 (秒)
     */
    long expireSeconds() default 10;

    /**
     * 是否等待上个执行结束后再放行
     */
    boolean waitingLast() default false;

    /**
     * 提示信息
     */
    String message() default "请勿重复提交";
}
