package org.jeecg.modules.assessscore.job;

import org.jeecg.modules.assessscore.service.IAssessScoreService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

public class AssessScoreJob implements Job {

    @Autowired
    private IAssessScoreService assessScoreService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        assessScoreService.modifyUnappraisal();
    }
}
