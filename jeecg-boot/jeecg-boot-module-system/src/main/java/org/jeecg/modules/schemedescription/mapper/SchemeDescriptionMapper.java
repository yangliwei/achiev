package org.jeecg.modules.schemedescription.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 程度描述表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface SchemeDescriptionMapper extends BaseMapper<SchemeDescription> {

    List<SchemeDescription> queryNotDeletedSchemeDescriptionType2();

}
