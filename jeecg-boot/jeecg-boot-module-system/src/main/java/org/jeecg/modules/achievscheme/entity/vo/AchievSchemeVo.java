package org.jeecg.modules.achievscheme.entity.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jeecg.modules.achievscheme.entity.AchievScheme;
import org.jeecg.modules.schemecontent.entity.vo.SchemeContentVo;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class AchievSchemeVo extends AchievScheme {

    List<SchemeContentVo> schemeContentList;

}
