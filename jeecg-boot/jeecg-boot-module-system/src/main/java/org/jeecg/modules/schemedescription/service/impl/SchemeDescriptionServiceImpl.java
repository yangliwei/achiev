package org.jeecg.modules.schemedescription.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import org.jeecg.modules.schemedescription.mapper.SchemeDescriptionMapper;
import org.jeecg.modules.schemedescription.service.ISchemeDescriptionService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 程度描述表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Service
public class SchemeDescriptionServiceImpl extends ServiceImpl<SchemeDescriptionMapper, SchemeDescription> implements ISchemeDescriptionService {

    @Override
    public List<SchemeDescription> querySchemeDescriptionBySchemeId(String id) {
        return this.list(new LambdaQueryWrapper<SchemeDescription>().eq(SchemeDescription::getSchemeId, id));
    }

    @Override
    public List<SchemeDescription> queryNotDeletedSchemeDescriptionType2() {
        return baseMapper.queryNotDeletedSchemeDescriptionType2();
    }
}
