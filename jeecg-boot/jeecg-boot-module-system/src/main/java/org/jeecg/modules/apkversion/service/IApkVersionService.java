package org.jeecg.modules.apkversion.service;

import org.jeecg.modules.apkversion.entity.ApkVersion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: app版本记录
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
public interface IApkVersionService extends IService<ApkVersion> {

    ApkVersion queryNewOne();
}
