package org.jeecg.modules.achievassess.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 绩效考评总表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Data
@TableName("achiev_assess")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="achiev_assess对象", description="绩效考评总表")
public class AchievAssess implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**方案id*/
	@Excel(name = "方案id", width = 15)
    @ApiModelProperty(value = "方案id")
    private java.lang.String schemeId;
	/**受评人*/
	@Excel(name = "受评人", width = 15)
    @ApiModelProperty(value = "受评人")
    private java.lang.String assessAppraisee;
	/**受评人名称*/
	@Excel(name = "受评人名称", width = 15)
    @ApiModelProperty(value = "受评人名称")
    private java.lang.String assessAppraiseeName;
	/**应完成数*/
	@Excel(name = "应完成数", width = 15)
    @ApiModelProperty(value = "应完成数")
    private java.lang.Double shouldCompletedCount;
	/**实际完成数*/
	@Excel(name = "实际完成数", width = 15)
    @ApiModelProperty(value = "实际完成数")
    private java.lang.Double actualCompletedCount;
	/**完成率*/
	@Excel(name = "完成率", width = 15)
    @ApiModelProperty(value = "完成率")
    private java.lang.Double completionRate;
	/**评分制合计*/
	@Excel(name = "评分制合计", width = 15)
    @ApiModelProperty(value = "评分制合计")
    private java.lang.Double scoringCount;
	/**合计*/
	@Excel(name = "合计", width = 15)
    @ApiModelProperty(value = "合计")
    private java.lang.Double assessTotal;
}
