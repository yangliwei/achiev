package org.jeecg.modules.schemedescription.service;

import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 程度描述表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface ISchemeDescriptionService extends IService<SchemeDescription> {

    /**
     * 查询对应schemeId下的程度描述
     * @param id
     * @return
     */
    List<SchemeDescription> querySchemeDescriptionBySchemeId(String id);

    List<SchemeDescription> queryNotDeletedSchemeDescriptionType2();

}
