package org.jeecg.modules.industry.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.industry.entity.Industry;

/**
 * @Description: 行业表
 * @Author: jeecg-boot
 * @Date:   2021-08-26
 * @Version: V1.0
 */
public interface IndustryMapper extends BaseMapper<Industry> {

}
