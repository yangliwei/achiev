package org.jeecg.modules.enums;

/**
 * @Author: admin
 * @Date: 2022-03-29 10:29
 */
public enum BelongToEnum {


    PINGAN("平安","001"),
    PICC("人保","002");


    private String code;

    private String name;

    private BelongToEnum(String name,String code){
        this.setCode(code);
        this.setName(name);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
