package org.jeecg.modules.assessdescription.service.impl;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.assessdescription.entity.AssessDescription;
import org.jeecg.modules.assessdescription.entity.vo.AssessDescriptionVo;
import org.jeecg.modules.assessdescription.mapper.AssessDescriptionMapper;
import org.jeecg.modules.assessdescription.service.IAssessDescriptionService;
import org.jeecg.modules.assessscore.entity.AssessScore;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 考评描述表
 * @Author: jeecg-boot
 * @Date:   2023-02-14
 * @Version: V1.0
 */
@Service
public class AssessDescriptionServiceImpl extends ServiceImpl<AssessDescriptionMapper, AssessDescription> implements IAssessDescriptionService {

    @Override
    public void generateAssessDescription(String schemeDescriptionId, String assessContentId, String achievAssessId, Map<String, List<AssessDescription>> assessDescriptionMap) {
        AssessDescription assessDescription = new AssessDescription();
        assessDescription.setSchemeDescriptionId(schemeDescriptionId);
        assessDescription.setAssessContentId(assessContentId);
        assessDescription.setAssessId(achievAssessId);
        List<AssessDescription> assessDescriptionList = assessDescriptionMap.get(assessContentId);
        if (ObjectUtils.isEmpty(assessDescriptionList)) {
            this.save(assessDescription);
            List<AssessDescription> assessScoreMapList = new ArrayList<>();
            assessScoreMapList.add(assessDescription);
            assessDescriptionMap.put(assessContentId, assessScoreMapList);
        } else {
            // 如果本月内已经存在该考评id,考评内容id和方案描述id的数据, 则不进行添加
            boolean parallel = assessDescriptionList.stream().anyMatch(item -> achievAssessId.equals(item.getAssessId()) && assessContentId.equals(item.getAssessContentId()) && StringUtils.equals(schemeDescriptionId, item.getSchemeDescriptionId()));
            if (!parallel) {
                this.save(assessDescription);
                List<AssessDescription> assessDescriptionMapList = assessDescriptionMap.get(assessContentId);
                assessDescriptionMapList.add(assessDescription);
                assessDescriptionMap.put(assessContentId, assessDescriptionMapList);
            }
        }
    }

    @Override
    public List<AssessDescriptionVo> queryVoBySchemeId(String schemeId) {
        List<AssessDescriptionVo> assessDescriptionVoList = baseMapper.queryVoBySchemeId(schemeId);
        Map<String, AssessDescriptionVo> assessDescriptionVoMap = assessDescriptionVoList.stream().filter(item->StringUtils.isNotEmpty(item.getId())).collect(Collectors.toMap(k -> k.getId() + "_" + k.getSchemeDescriptionId(), v -> v));
        List<AssessDescriptionVo> tempData = baseMapper.queryData(schemeId);
        List<AssessDescriptionVo> result = assessDescriptionVoList.stream().filter(item -> StringUtils.isEmpty(item.getId())).collect(Collectors.toList());
        for (AssessDescriptionVo item : tempData) {
            String index = item.getId() + "_" + item.getSchemeDescriptionId();
            AssessDescriptionVo assessDescriptionVo = assessDescriptionVoMap.get(index);
            if (ObjectUtils.isNotEmpty(assessDescriptionVo)) {
                result.add(assessDescriptionVo);
            }
        }
        return result;
    }
}
