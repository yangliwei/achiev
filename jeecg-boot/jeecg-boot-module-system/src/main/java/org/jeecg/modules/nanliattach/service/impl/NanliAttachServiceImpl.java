package org.jeecg.modules.nanliattach.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.modules.nanliattach.entity.NanliAttach;
import org.jeecg.modules.nanliattach.mapper.NanliAttachMapper;
import org.jeecg.modules.nanliattach.service.INanliAttachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 附件表
 * @Author: risk-manage
 * @Date:   2021-10-27
 * @Version: V1.0
 */
@Service
public class NanliAttachServiceImpl extends ServiceImpl<NanliAttachMapper, NanliAttach> implements INanliAttachService {

    @Autowired
    private NanliAttachMapper nanliAttachMapper;

    @Override
    public String getUrlById(String id) {
        return nanliAttachMapper.getUrlById(id);


    }

    @Override
    public List<JSONObject> getUrlList(String ids) {
        return nanliAttachMapper.getUrlList(ids);
    }

    @Override
    public List<String> getByUrlList(String[] urls) {
        List<String> byUrlList = nanliAttachMapper.getByUrlList(urls);
        return byUrlList;
    }
}
