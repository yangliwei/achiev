package org.jeecg.modules.industrymanage.service.impl;

import org.jeecg.modules.industrymanage.entity.IndustryManage;
import org.jeecg.modules.industrymanage.mapper.IndustryManageMapper;
import org.jeecg.modules.industrymanage.service.IIndustryManageService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 行业管理
 * @Author: jeecg-boot
 * @Date:   2021-07-04
 * @Version: V1.0
 */
@Service
public class IndustryManageServiceImpl extends ServiceImpl<IndustryManageMapper, IndustryManage> implements IIndustryManageService {

}
