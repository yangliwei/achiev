package org.jeecg.modules.achievscheme.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 考评方案总案
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Data
@TableName("achiev_scheme")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="achiev_scheme对象", description="考评方案总案")
public class AchievScheme implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**方案名*/
	@Excel(name = "方案名", width = 15)
    @ApiModelProperty(value = "方案名")
    private java.lang.String schemeName;
	/**完成率描述*/
	@Excel(name = "完成率描述", width = 15)
    @ApiModelProperty(value = "完成率描述")
    private java.lang.String completedContent;
	/**应完成描述*/
	@Excel(name = "应完成描述", width = 15)
    @ApiModelProperty(value = "应完成描述")
    private java.lang.String shouldCompletedContent;
	/**实际完成描述*/
	@Excel(name = "实际完成描述", width = 15)
    @ApiModelProperty(value = "实际完成描述")
    private java.lang.String actualCompletedContent;
	/**完成率考评人*/
	@Excel(name = "完成率考评人", width = 15)
    @ApiModelProperty(value = "完成率考评人")
    private java.lang.String completedAppraiser;
	/**完成率考评人名称*/
	@Excel(name = "完成率考评人名称", width = 15)
    @ApiModelProperty(value = "完成率考评人名称")
    private java.lang.String completedAppraiserName;
    /**是否删除*/
    @Excel(name = "是否删除", width = 15)
    @TableLogic
    @ApiModelProperty(value = "是否删除")
    private boolean deleted;

}
