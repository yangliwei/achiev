package org.jeecg.modules.apkversion.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.apkversion.entity.ApkVersion;
import org.jeecg.modules.apkversion.service.IApkVersionService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: app版本记录
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Api(tags = "app版本记录")
@RestController
@RequestMapping("/apkversion/apkVersion")
@Slf4j
public class ApkVersionController extends JeecgController<ApkVersion, IApkVersionService> {
    @Autowired
    private IApkVersionService apkVersionService;

    /**
     * 分页列表查询
     *
     * @param apkVersion
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "app版本记录-分页列表查询")
    @ApiOperation(value = "app版本记录-分页列表查询", notes = "app版本记录-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ApkVersion apkVersion,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ApkVersion> queryWrapper = QueryGenerator.initQueryWrapper(apkVersion, req.getParameterMap());
        Page<ApkVersion> page = new Page<ApkVersion>(pageNo, pageSize);
        IPage<ApkVersion> pageList = apkVersionService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param apkVersion
     * @return
     */
    @AutoLog(value = "app版本记录-添加")
    @ApiOperation(value = "app版本记录-添加", notes = "app版本记录-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ApkVersion apkVersion) {
        apkVersionService.save(apkVersion);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param apkVersion
     * @return
     */
    @AutoLog(value = "app版本记录-编辑")
    @ApiOperation(value = "app版本记录-编辑", notes = "app版本记录-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ApkVersion apkVersion) {
        apkVersionService.updateById(apkVersion);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "app版本记录-通过id删除")
    @ApiOperation(value = "app版本记录-通过id删除", notes = "app版本记录-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        apkVersionService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "app版本记录-批量删除")
    @ApiOperation(value = "app版本记录-批量删除", notes = "app版本记录-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.apkVersionService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "app版本记录-通过id查询")
    @ApiOperation(value = "app版本记录-通过id查询", notes = "app版本记录-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ApkVersion apkVersion = apkVersionService.getById(id);
        if (apkVersion == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(apkVersion);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param apkVersion
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ApkVersion apkVersion) {
        return super.exportXls(request, apkVersion, ApkVersion.class, "app版本记录");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ApkVersion.class);
    }

    @GetMapping(value = "/queryNewOne")
    public Result<?> queryNewOne() {
        ApkVersion apkVersion = apkVersionService.queryNewOne();
        return Result.OK(apkVersion);
    }

}
