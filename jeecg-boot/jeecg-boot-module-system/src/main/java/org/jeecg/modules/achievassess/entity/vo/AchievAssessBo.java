package org.jeecg.modules.achievassess.entity.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jeecg.modules.achievassess.entity.AchievAssess;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.util.Date;


@Data
public class AchievAssessBo {
    /**
     * 被评人名字
     */
    @Excel(name = "被评人名字", width = 15)
    private String assessAppraiseeName;

    /**
     * 分数
     */
    @Excel(name = "评分数", width = 15)
    private Double scoringCount;

    /**创建日期*/
    @Excel(name = "年月", width = 15)
    private String createTime;

    /**
     *部门名称
     */
    @Excel(name = "部门名称", width = 15)
    private  String depatmentname;
}
