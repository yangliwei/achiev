package org.jeecg.modules.aspect;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.ObjectUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.jeecg.modules.utils.RemoveBlankUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

@Aspect
@Component
public class RemoveBlankAspect {

    @Pointcut("@annotation(org.jeecg.modules.annotation.RemoveBlank)")
    public void RemoveBlank() {
    }

    /**
     * 环绕通知 （可以控制目标方法前中后期执行操作，目标方法执行前后分别执行一些代码）
     *
     * @param joinPoint
     * @return
     */
    @Around("RemoveBlank()")
    public Object before(ProceedingJoinPoint joinPoint) throws Exception {
        //获取执行方法
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();

        Object[] args = joinPoint.getArgs();
        Class<?>[] parameterTypes = method.getParameterTypes();

        for (int i = 0; i < args.length; i++) {
            try {
                if (ObjectUtils.isNotEmpty(args[i])) {
                    if (args[i] instanceof JSONObject) {
                        args[i] = RemoveBlankUtils.jsonRemoveBlank((JSONObject) args[i]);
                    } else if (args[i] instanceof String) {
                        args[i] = ((String) args[i]).trim();
                    } else if (args[i] instanceof List<?>) {
                        args[i] = RemoveBlankUtils.listRemoveBlank(args[i], parameterTypes[i]);
                    } else if (args[i] instanceof Set<?>) {
                        args[i] = RemoveBlankUtils.setRemoveBlank(args[i], parameterTypes[i]);
                    } else if (!isJavaClass(parameterTypes[i])) {
                        args[i] = RemoveBlankUtils.entityRemoveBlank(args[i], parameterTypes[i]);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            //正常执行方法并返回
            //ProceedingJoinPoint类型参数可以决定是否执行目标方法，且环绕通知必须要有返回值，返回值即为目标方法的返回值
            return joinPoint.proceed(args);
        } catch (Throwable throwable) {
            //确保方法执行异常实时释放限时标记(异常后置通知)
            throw new RuntimeException(throwable);
        }
    }

    /**
     * 判断类是Java类型还是自定义类型
     * @param clazz 需要判断的类
     * @return
     */
    private static boolean isJavaClass(Class<?> clazz) {
        return clazz != null && clazz.getClassLoader() == null;
    }
}
