package org.jeecg.modules.assesscontent.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.models.Scheme;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.assesscontent.entity.AssessContent;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentGroup;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentVo;
import org.jeecg.modules.assesscontent.mapper.AssessContentMapper;
import org.jeecg.modules.assesscontent.service.IAssessContentService;
import org.jeecg.modules.assessdescription.entity.vo.AssessDescriptionVo;
import org.jeecg.modules.assessdescription.service.IAssessDescriptionService;
import org.jeecg.modules.assessscore.entity.AssessScore;
import org.jeecg.modules.assessscore.service.IAssessScoreService;
import org.jeecg.modules.schemecontent.entity.SchemeContent;
import org.jeecg.modules.schemecontent.service.ISchemeContentService;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import org.jeecg.modules.schemedescription.service.ISchemeDescriptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 考评内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Service
public class AssessContentServiceImpl extends ServiceImpl<AssessContentMapper, AssessContent> implements IAssessContentService {

    @Autowired
    private IAssessScoreService assessScoreService;
    @Autowired
    private ISchemeContentService schemeContentService;
    @Autowired
    private ISchemeDescriptionService schemeDescriptionService;
    @Autowired
    private IAssessDescriptionService assessDescriptionService;

    @Override
    public List<AssessContentVo> queryAssessContentVoByAssessId(String id, String schemeId) {
        // 查询该schemeId下的内容
        List<AssessContent> assessContentList = this.list(new LambdaQueryWrapper<AssessContent>().eq(AssessContent::getAssessId, id));
        Map<String, List<AssessScore>> assessScoreMap = assessScoreService.list(new LambdaQueryWrapper<AssessScore>().eq(AssessScore::getAssessId, id)).stream().collect(Collectors.groupingBy(AssessScore::getAssessContentId));
        // 查询方案考评内容的数据进行vo封装
        Map<String, SchemeContent> schemeContentMap = schemeContentService.list(new LambdaQueryWrapper<SchemeContent>().eq(SchemeContent::getSchemeId, schemeId)).stream().collect(Collectors.toMap(SchemeContent::getId, t -> t));
        List<AssessDescriptionVo> assessDescriptionVos = assessDescriptionService.queryVoBySchemeId(schemeId).stream().filter(item-> StringUtils.isNotBlank(item.getAssessContentId())).collect(Collectors.toList());
        // 查询方案程度描述的内容数据进行vo封装
        Map<String, List<AssessDescriptionVo>> assessDescriptionVoMap = ObjectUtils.isEmpty(assessDescriptionVos) ? new HashMap<>() : assessDescriptionVos.stream().collect(Collectors.groupingBy(AssessDescriptionVo::getAssessContentId));
        return assessContentList.stream().map(item -> {
            AssessContentVo assessContentVo = new AssessContentVo();
            BeanUtils.copyProperties(item, assessContentVo);
            assessContentVo.setSchemeContent(schemeContentMap.get(item.getSchemeContentId()).getSchemeContent());
            assessContentVo.setSort(schemeContentMap.get(item.getSchemeContentId()).getSort());
            assessContentVo.setAssessScoreList(assessScoreMap.get(item.getId()));
            assessContentVo.setAssessDescriptionList(assessDescriptionVoMap.get(item.getId()));
            return assessContentVo;
        }).sorted(Comparator.comparingInt(AssessContentVo::getSort)).collect(Collectors.toList());
    }

    @Override
    public List<AssessContentVo> queryAssessContentVoByAssessId(String id, String schemeId, String userId) {
        List<AssessContent> assessContentList = this.list(new LambdaQueryWrapper<AssessContent>().eq(AssessContent::getAssessId, id));
        Map<String, List<AssessScore>> assessScoreMap = assessScoreService.list(new LambdaQueryWrapper<AssessScore>().eq(AssessScore::getAssessId, id)).stream().collect(Collectors.groupingBy(AssessScore::getAssessContentId));
        Map<String, SchemeContent> schemeContentMap = schemeContentService.list(new LambdaQueryWrapper<SchemeContent>().eq(SchemeContent::getSchemeId, schemeId)).stream().collect(Collectors.toMap(SchemeContent::getId, t->t));
        List<AssessDescriptionVo> assessDescriptionVos = assessDescriptionService.queryVoBySchemeId(schemeId).stream().filter(item-> StringUtils.isNotBlank(item.getAssessContentId())).collect(Collectors.toList());
        Map<String, List<AssessDescriptionVo>> assessDescriptionVoMap = ObjectUtils.isEmpty(assessDescriptionVos) ? new HashMap<>() : assessDescriptionVos.stream().collect(Collectors.groupingBy(AssessDescriptionVo::getAssessContentId));
        return assessContentList.stream().map(item -> {
            AssessContentVo assessContentVo = new AssessContentVo();
            BeanUtils.copyProperties(item, assessContentVo);
            assessContentVo.setSchemeContent(schemeContentMap.get(item.getSchemeContentId()).getSchemeContent());
            assessContentVo.setSort(schemeContentMap.get(item.getSchemeContentId()).getSort());
            List<AssessScore> assessScoreList = assessScoreMap.get(item.getId());
            List<String> scoreAppraiserList = assessScoreList.stream().map(AssessScore::getScoreAppraiser).collect(Collectors.toList());
            // 判断用户id是否包含在该考评内容的考评人里
            if (scoreAppraiserList.contains(userId)) {
                // 只返回与用户id匹配上的评分数据
                List<AssessScore> assessScores = assessScoreMap.get(item.getId()).stream().filter(obj -> obj.getScoreAppraiser().equals(userId)).collect(Collectors.toList());
                assessContentVo.setAssessScoreList(assessScores);
                assessContentVo.setAssessDescriptionList(assessDescriptionVoMap.get(item.getId()));
                return assessContentVo;
            } else {
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public void updateAssessContentAvg(String id) {
        baseMapper.updateBatchAssessContentAvg(Collections.singletonList(id));
    }

    @Override
    public List<AssessContentGroup> AssessContentListGroup(List<AssessContentVo> assessContentVoList) {
        Map<String, List<AssessContentVo>> assessContentGroupMap = assessContentVoList.stream().collect(Collectors.groupingBy(AssessContentVo::getSchemeContent));
        List<Map.Entry<String, List<AssessContentVo>>>list = new ArrayList<>(assessContentGroupMap.entrySet());
        list.sort(Comparator.comparingInt(me -> me.getValue().get(0).getSort()));
        List<AssessContentGroup> groupList = new ArrayList<>();
        for (Map.Entry<String, List<AssessContentVo>> stringListEntry : list) {
            String k = stringListEntry.getKey();
            List<AssessContentVo> v = stringListEntry.getValue();
            AssessContentGroup assessContentGroup = new AssessContentGroup();
            assessContentGroup.setSchemeContent(k);
            assessContentGroup.setAssessContentVoList(v);
            groupList.add(assessContentGroup);
        }
        return groupList;
    }

    @Override
    public void updateBatchAssessContentAvg(List<String> list) {
        baseMapper.updateBatchAssessContentAvg(list);
    }

    @Override
    public List<AssessContent> queryWithoutScoreDataList() {
        return baseMapper.queryWithoutScoreDataList();
    }
}
