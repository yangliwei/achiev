package org.jeecg.modules.schemecontent.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.schemecontent.entity.SchemeContent;
import org.jeecg.modules.schemecontent.service.ISchemeContentService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 方案内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Api(tags="方案内容表")
@RestController
@RequestMapping("/schemecontent/schemeContent")
@Slf4j
public class SchemeContentController extends JeecgController<SchemeContent, ISchemeContentService> {
	@Autowired
	private ISchemeContentService schemeContentService;
	
	/**
	 * 分页列表查询
	 *
	 * @param schemeContent
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "方案内容表-分页列表查询")
	@ApiOperation(value="方案内容表-分页列表查询", notes="方案内容表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SchemeContent schemeContent,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SchemeContent> queryWrapper = QueryGenerator.initQueryWrapper(schemeContent, req.getParameterMap());
		Page<SchemeContent> page = new Page<SchemeContent>(pageNo, pageSize);
		IPage<SchemeContent> pageList = schemeContentService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param schemeContent
	 * @return
	 */
	@AutoLog(value = "方案内容表-添加")
	@ApiOperation(value="方案内容表-添加", notes="方案内容表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SchemeContent schemeContent) {
		schemeContentService.save(schemeContent);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param schemeContent
	 * @return
	 */
	@AutoLog(value = "方案内容表-编辑")
	@ApiOperation(value="方案内容表-编辑", notes="方案内容表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SchemeContent schemeContent) {
		schemeContentService.updateById(schemeContent);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "方案内容表-通过id删除")
	@ApiOperation(value="方案内容表-通过id删除", notes="方案内容表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		schemeContentService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "方案内容表-批量删除")
	@ApiOperation(value="方案内容表-批量删除", notes="方案内容表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.schemeContentService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "方案内容表-通过id查询")
	@ApiOperation(value="方案内容表-通过id查询", notes="方案内容表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SchemeContent schemeContent = schemeContentService.getById(id);
		if(schemeContent==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(schemeContent);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param schemeContent
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SchemeContent schemeContent) {
        return super.exportXls(request, schemeContent, SchemeContent.class, "方案内容表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, SchemeContent.class);
    }

}
