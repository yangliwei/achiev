package org.jeecg.modules.assesscontent.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.assesscontent.entity.AssessContent;
import org.jeecg.modules.assesscontent.service.IAssessContentService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 考评内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Api(tags="考评内容表")
@RestController
@RequestMapping("/assesscontent/assessContent")
@Slf4j
public class AssessContentController extends JeecgController<AssessContent, IAssessContentService> {
	@Autowired
	private IAssessContentService assessContentService;
	
	/**
	 * 分页列表查询
	 *
	 * @param assessContent
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "考评内容表-分页列表查询")
	@ApiOperation(value="考评内容表-分页列表查询", notes="考评内容表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(AssessContent assessContent,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<AssessContent> queryWrapper = QueryGenerator.initQueryWrapper(assessContent, req.getParameterMap());
		Page<AssessContent> page = new Page<AssessContent>(pageNo, pageSize);
		IPage<AssessContent> pageList = assessContentService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param assessContent
	 * @return
	 */
	@AutoLog(value = "考评内容表-添加")
	@ApiOperation(value="考评内容表-添加", notes="考评内容表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody AssessContent assessContent) {
		assessContentService.save(assessContent);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param assessContent
	 * @return
	 */
	@AutoLog(value = "考评内容表-编辑")
	@ApiOperation(value="考评内容表-编辑", notes="考评内容表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody AssessContent assessContent) {
		assessContentService.updateById(assessContent);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "考评内容表-通过id删除")
	@ApiOperation(value="考评内容表-通过id删除", notes="考评内容表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		assessContentService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "考评内容表-批量删除")
	@ApiOperation(value="考评内容表-批量删除", notes="考评内容表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.assessContentService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "考评内容表-通过id查询")
	@ApiOperation(value="考评内容表-通过id查询", notes="考评内容表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		AssessContent assessContent = assessContentService.getById(id);
		if(assessContent==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(assessContent);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param assessContent
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, AssessContent assessContent) {
        return super.exportXls(request, assessContent, AssessContent.class, "考评内容表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, AssessContent.class);
    }

}
