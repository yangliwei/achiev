package org.jeecg.modules.achievassess.entity.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jeecg.modules.achievassess.entity.AchievAssess;
import org.jeecg.modules.assesscontent.entity.AssessContent;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentGroup;
import org.jeecg.modules.assesscontent.entity.vo.AssessContentVo;
import org.jeecg.modules.schemecontent.entity.vo.SchemeContentVo;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class AchievAssessVo extends AchievAssess {

    private String schemeName;
    private String departId;
    private String completedContent;
    private String shouldCompletedContent;
    private String actualCompletedContent;
    private String completedAppraiser;
    private String completedAppraiserName;
    private String completionRateTips;
    private String scoreTotalTips;
    private List<AssessContentGroup> assessContentGroup;

    private Double isOperated;
}