package org.jeecg.modules.nanliattach.controller;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.nanliattach.entity.NanliAttach;
import org.jeecg.modules.nanliattach.service.INanliAttachService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 附件表
 * @Author: risk-manage
 * @Date:   2021-10-27
 * @Version: V1.0
 */
@Api(tags="附件表")
@RestController
@RequestMapping("/nanliattach/nanliAttach")
@Slf4j
public class NanliAttachController extends JeecgController<NanliAttach, INanliAttachService> {
	@Autowired
	private INanliAttachService hseAttachService;
	
	/**
	 * 分页列表查询
	 *
	 * @param nanliAttach
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "附件表-分页列表查询")
	@ApiOperation(value="附件表-分页列表查询", notes="附件表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(NanliAttach nanliAttach,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<NanliAttach> queryWrapper = QueryGenerator.initQueryWrapper(nanliAttach, req.getParameterMap());
		Page<NanliAttach> page = new Page<NanliAttach>(pageNo, pageSize);
		IPage<NanliAttach> pageList = hseAttachService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param nanliAttach
	 * @return
	 */
	@AutoLog(value = "附件表-添加")
	@ApiOperation(value="附件表-添加", notes="附件表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody NanliAttach nanliAttach) {
		hseAttachService.save(nanliAttach);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param nanliAttach
	 * @return
	 */
	@AutoLog(value = "附件表-编辑")
	@ApiOperation(value="附件表-编辑", notes="附件表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody NanliAttach nanliAttach) {
		hseAttachService.updateById(nanliAttach);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "附件表-通过id删除")
	@ApiOperation(value="附件表-通过id删除", notes="附件表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		hseAttachService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "附件表-批量删除")
	@ApiOperation(value="附件表-批量删除", notes="附件表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.hseAttachService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "附件表-通过id查询")
	@ApiOperation(value="附件表-通过id查询", notes="附件表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		NanliAttach nanliAttach = hseAttachService.getById(id);
		if(nanliAttach ==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(nanliAttach);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param nanliAttach
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, NanliAttach nanliAttach) {
        return super.exportXls(request, nanliAttach, NanliAttach.class, "附件表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, NanliAttach.class);
    }

	 /**
	  * 通过id查询
	  *
	  * @param id
	  * @return
	  */
	 @AutoLog(value = "附件表-通过附件id查询附件地址")
	 @ApiOperation(value="附件表-通过附件id查询附件地址", notes="附件表-通过附件id查询附件地址")
	 @GetMapping(value = "/queryUrl")
	 public String queryUrl(@RequestParam(name="id",required=true) String id) {
		 String url = hseAttachService.getUrlById(id);
		 if(url==null) {
			 return "未找到数据";
		 }
		 return url;
	 }

	 /**
	  * 通过id查询
	  *
	  * @param id
	  * @return
	  */
	 @AutoLog(value = "附件表-通过附件id查询附件地址")
	 @ApiOperation(value="附件表-通过附件id查询附件地址", notes="附件表-通过附件id查询附件地址")
	 @GetMapping(value = "/queryUrlById")
	 public JSONObject queryUrlById(@RequestParam(name="id",required=true) String id) {
	 	JSONObject result = new JSONObject();
		 String url = hseAttachService.getUrlById(id);
		 result.put("id",id);
		 result.put("url",url);
		 return result;
	 }

	 /**
	  * 通过id查询
	  *
	  * @param ids
	  * @return
	  */
	 @AutoLog(value = "附件表-通过附件id查询附件地址")
	 @ApiOperation(value="附件表-通过附件id查询附件地址", notes="附件表-通过附件id查询附件地址")
	 @GetMapping(value = "/queryUrlList")
	 public List<JSONObject> queryUrlList(@RequestParam(name="ids",required=true) String ids) {

		 List<JSONObject> urls = hseAttachService.getUrlList("("+ids+")");

		 return urls;
	 }


}
