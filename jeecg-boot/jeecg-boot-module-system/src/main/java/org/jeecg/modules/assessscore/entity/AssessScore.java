package org.jeecg.modules.assessscore.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 评分表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Data
@TableName("assess_score")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="assess_score对象", description="评分表")
public class AssessScore implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**考评id*/
	@Excel(name = "考评id", width = 15)
    @ApiModelProperty(value = "考评id")
    private java.lang.String assessId;
	/**考评内容id*/
	@Excel(name = "考评内容id", width = 15)
    @ApiModelProperty(value = "考评内容id")
    private java.lang.String assessContentId;
	/**考评人*/
	@Excel(name = "考评人", width = 15)
    @ApiModelProperty(value = "考评人")
    private java.lang.String scoreAppraiser;
	/**考评人名称*/
	@Excel(name = "考评人名称", width = 15)
    @ApiModelProperty(value = "考评人名称")
    private java.lang.String scoreAppraiserName;
	/**分值*/
	@Excel(name = "分值", width = 15)
    @ApiModelProperty(value = "分值")
    private java.lang.Double scoreTotal;
}
