package org.jeecg.modules.schemeappraiser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import org.jeecg.modules.schemeappraiser.mapper.SchemeAppraiserMapper;
import org.jeecg.modules.schemeappraiser.service.ISchemeAppraiserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 方案考评人表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Service
public class SchemeAppraiserServiceImpl extends ServiceImpl<SchemeAppraiserMapper, SchemeAppraiser> implements ISchemeAppraiserService {

    @Override
    public List<SchemeAppraiser> querySchemeSchemeAppraiserBySchemeId(String id) {
        return this.list(new LambdaQueryWrapper<SchemeAppraiser>().eq(SchemeAppraiser::getSchemeId, id));
    }

    @Override
    public List<SchemeAppraiser> queryNotDeletedSchemeAppraiser() {
        return baseMapper.queryNotDeletedSchemeAppraiser();
    }

    @Override
    public List<SchemeAppraiser> listByAppraiser(String id) {
        return this.list(new LambdaQueryWrapper<SchemeAppraiser>().eq(SchemeAppraiser::getSchemeAppraiser, id));
    }
}
