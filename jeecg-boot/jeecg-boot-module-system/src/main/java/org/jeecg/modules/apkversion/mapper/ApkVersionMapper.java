package org.jeecg.modules.apkversion.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.apkversion.entity.ApkVersion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: app版本记录
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
public interface ApkVersionMapper extends BaseMapper<ApkVersion> {

}
