package org.jeecg.modules.achievscheme.service;

import org.jeecg.modules.achievscheme.entity.AchievScheme;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.achievscheme.entity.vo.AchievSchemeVo;

import java.util.List;

/**
 * @Description: 考评方案总案
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface IAchievSchemeService extends IService<AchievScheme> {

    /**
     * 查询方案表信息_根据id
     * @param id 方案总表id
     * @return
     */
    AchievSchemeVo querySchemeById(String id);

    /**
     * 新增或修改考评表模板
     * @param achievSchemeVo 数据
     * @return
     */
    void saveOrUpdateScheme(AchievSchemeVo achievSchemeVo);

    /**
     * 根据考评人id查询由他负责完成率考评的方案
     * @param id
     * @return
     */
    List<AchievScheme> listByAppraiser(String id);

}
