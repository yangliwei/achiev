package org.jeecg.modules.industry.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.industry.entity.Industry;

/**
 * @Description: 行业表
 * @Author: jeecg-boot
 * @Date: 2021-08-26
 * @Version: V1.0
 */
public interface IIndustryService extends IService<Industry> {

    Industry queryByIndustryCode(String code);

}
