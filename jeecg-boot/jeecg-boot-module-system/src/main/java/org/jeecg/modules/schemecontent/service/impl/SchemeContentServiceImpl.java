package org.jeecg.modules.schemecontent.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import org.jeecg.modules.schemeappraiser.service.ISchemeAppraiserService;
import org.jeecg.modules.schemecontent.entity.SchemeContent;
import org.jeecg.modules.schemecontent.entity.vo.SchemeContentVo;
import org.jeecg.modules.schemecontent.mapper.SchemeContentMapper;
import org.jeecg.modules.schemecontent.service.ISchemeContentService;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import org.jeecg.modules.schemedescription.service.ISchemeDescriptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 方案内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Service
public class SchemeContentServiceImpl extends ServiceImpl<SchemeContentMapper, SchemeContent> implements ISchemeContentService {

    @Autowired
    private ISchemeDescriptionService schemeDescriptionService;
    @Autowired
    private ISchemeAppraiserService schemeAppraiserService;

    @Override
    public List<SchemeContentVo> querySchemeContentVoBySchemeId(String id) {
        // 查询该schemeId对应的考评内容
        List<SchemeContent> schemeContentList = this.list(new LambdaQueryWrapper<SchemeContent>().eq(SchemeContent::getSchemeId, id));
        // 提前查询对应schemeId下的程度描述以及考评人
        List<SchemeDescription> schemeDescriptionList = schemeDescriptionService.querySchemeDescriptionBySchemeId(id);
        Map<String, List<SchemeDescription>> schemeDescriptionMap = schemeDescriptionList.stream().collect(Collectors.groupingBy(SchemeDescription::getSchemeContentId));
        List<SchemeAppraiser> schemeAppraiserList = schemeAppraiserService.querySchemeSchemeAppraiserBySchemeId(id);
        Map<String, List<SchemeAppraiser>> schemeAppraiserMap = schemeAppraiserList.stream().collect(Collectors.groupingBy(SchemeAppraiser::getSchemeContentId));
        return schemeContentList.stream().map(item -> {
            SchemeContentVo schemeContentVo = new SchemeContentVo();
            BeanUtils.copyProperties(item, schemeContentVo);
            // 封装程度描述
            schemeContentVo.setSchemeDescriptionList(schemeDescriptionMap.get(item.getId()));
            // 封装考评人
            schemeContentVo.setSchemeAppraiserList(schemeAppraiserMap.get(item.getId()));
            return schemeContentVo;
        }).sorted(Comparator.comparingInt(SchemeContent::getSort)).collect(Collectors.toList());
    }

    @Override
    public List<SchemeContent> queryNotDeletedSchemeContent() {
        return baseMapper.queryNotDeletedSchemeContent();
    }
}
