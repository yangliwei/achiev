package org.jeecg.modules.industry.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.modules.industry.entity.Industry;
import org.jeecg.modules.industry.mapper.IndustryMapper;
import org.jeecg.modules.industry.service.IIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 行业表
 * @Author: jeecg-boot
 * @Date: 2021-08-26
 * @Version: V1.0
 */
@Service
public class IndustryServiceImpl extends ServiceImpl<IndustryMapper, Industry> implements IIndustryService {

    @Autowired
    public IndustryMapper industryMapper;

    /**
     * 根据行业编码查询
     *
     * @param code
     * @return
     */
    @Override
    public Industry queryByIndustryCode(String code) {
        return industryMapper.selectOne(new QueryWrapper<Industry>().eq("industry_name", code));
    }
}
