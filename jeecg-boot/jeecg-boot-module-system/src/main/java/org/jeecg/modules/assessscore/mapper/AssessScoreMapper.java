package org.jeecg.modules.assessscore.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.assessscore.entity.AssessScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 评分表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface AssessScoreMapper extends BaseMapper<AssessScore> {

    /**
     * 查询有单人评分项未被评分的数据的id集合
     * @return
     */
    List<String> queryModifyUnappraisalAssessIdListSingle();

    /**
     * 修改有单人评分项未被评分的数据
     */
    void modifyUnappraisalSingle();

    /**
     * 查询有多人评分项未被评分的数据的id集合
     * @return
     */
    List<String> queryModifyUnappraisalAssessIdListMany();

    /**
     * 修改有多人评分项未被评分的数据
     */
    void modifyUnappraisalMany();
}
