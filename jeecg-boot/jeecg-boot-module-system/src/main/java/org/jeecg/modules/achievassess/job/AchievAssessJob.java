package org.jeecg.modules.achievassess.job;

import org.jeecg.modules.achievassess.service.IAchievAssessService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

public class AchievAssessJob implements Job {

    @Autowired
    private IAchievAssessService achievAssessService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        achievAssessService.generatePerformanceAppraisal();
    }
}
