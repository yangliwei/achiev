package org.jeecg.modules.pdf.controller;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.pdf.enumCode.PathEnumCode;
import org.jeecg.modules.pdf.util.PdfUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/sys/file/pdf")
@Slf4j
public class Word2PdfExport {

    /**
     * @param name
     * @param response
     * @throws Exception
     */
    @RequestMapping("/download/{name}")
    public void logDownload(@PathVariable String name, HttpServletResponse response) throws Exception {
        log.info(name + "********************");
        File file = new File("C:\\Users\\admin\\Desktop" + File.separator + name);
        log.info(file.getName(), file.getPath());
        if (!file.exists()) {
            log.error(name + "文件不存在");
        }
        response.setContentType("application/force-download");
        response.addHeader("Content-Disposition", "attachment;fileName=" + name);

        byte[] buffer = new byte[1024];
        try (FileInputStream fis = new FileInputStream(file);
             BufferedInputStream bis = new BufferedInputStream(fis)) {

            OutputStream os = response.getOutputStream();

            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer, 0, i);
                i = bis.read(buffer);
            }
        }
    }

    @GetMapping("/export")
    public Result<String> wordToPdfExport() {

        Result result = new Result();

        try {
            //替换文字信息
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("This is the unit", "广州市测试");
            //生产日期
            SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
            map.put("This is the date", format.format(new Date()));
            map.put("This is the Name of engineer", "张三");
            map.put("This is the address of the company", "等待企业信息生成公司地址");
            map.put("This is the company contact", "等待企业信息生成公司联系人");
            map.put("This is the contact number", "等待企业信息生成公司联系电话");
            map.put("This is a hand selected time", "这里需要等待操作员手选时间");

            List<String> strings = new ArrayList<String>();
            strings.add("火灾");
            strings.add("爆炸");
            strings.add("触电");
            strings.add("职业健康");
            strings.add("机械伤害");
            strings.add("化学品伤害");
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < strings.size(); i++) {
                if (i == 0) {
                    buffer.append(strings.get(i));
                } else {
                    buffer.append("、" + strings.get(i));
                }
            }
            map.put("It's multiple options", buffer.toString());
            /**
             * 根据在word文档中添加的书签进行图片插入，并不会覆盖其他信息
             */
            map.put("附件一", "C:\\Users\\admin\\Desktop\\测试图片.jpg");
            map.put("附件二", "C:\\Users\\admin\\Desktop\\测试图片.jpg");
            PdfUtil.searchAndReplace(PathEnumCode.FILE_WHERE_PATH.getPath(), PathEnumCode.WORDFILE_OUT_PATH.getPath(), map);

            //word转PDF
            PdfUtil.docToPdf( PathEnumCode.WORDFILE_OUT_PATH.getPath(),  PathEnumCode.PDFFILE_OUT_PATH.getPath());
            result.setMessage("导出完成");
        } catch (Exception e) {
            result.setMessage("导出失败");
        }
        return result;

    }

}
