package org.jeecg.modules.assessscore.service;

import org.jeecg.modules.assessscore.entity.AssessScore;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * @Description: 评分表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface IAssessScoreService extends IService<AssessScore> {

    /**
     * 生成考评人数据
     * @param achievAssessId
     * @param assessContentId
     * @param schemeAppraiserId
     * @param schemeAppraiserName
     * @param assessScoreMap
     * @param assessAppraisee
     */
    void generateAssessScore(String achievAssessId, String assessContentId, String schemeAppraiserId, String schemeAppraiserName, Map<String, List<AssessScore>> assessScoreMap, String assessAppraisee);

    /**
     * 查询该用户对应的特殊考评人的具体人列表
     * @param userId
     * @param schemeAppraiserName
     * @param departId
     * @return
     */
    List<SysUser> querySpecialAppraiser(String userId, String schemeAppraiserName, String departId);

    /**
     * 修改为评分的数据
     */
    void modifyUnappraisal();


}
