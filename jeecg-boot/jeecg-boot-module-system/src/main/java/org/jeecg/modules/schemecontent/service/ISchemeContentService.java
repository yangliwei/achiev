package org.jeecg.modules.schemecontent.service;

import org.jeecg.modules.schemecontent.entity.SchemeContent;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.schemecontent.entity.vo.SchemeContentVo;

import java.util.List;

/**
 * @Description: 方案内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface ISchemeContentService extends IService<SchemeContent> {

    /**
     * 查询出页面所需展示的第二层的数据
     * @param id
     * @return
     */
    List<SchemeContentVo> querySchemeContentVoBySchemeId(String id);

    List<SchemeContent> queryNotDeletedSchemeContent();

}
