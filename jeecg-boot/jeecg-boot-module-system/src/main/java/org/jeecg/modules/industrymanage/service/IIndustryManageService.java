package org.jeecg.modules.industrymanage.service;

import org.jeecg.modules.industrymanage.entity.IndustryManage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 行业管理
 * @Author: jeecg-boot
 * @Date:   2021-07-04
 * @Version: V1.0
 */
public interface IIndustryManageService extends IService<IndustryManage> {

}
