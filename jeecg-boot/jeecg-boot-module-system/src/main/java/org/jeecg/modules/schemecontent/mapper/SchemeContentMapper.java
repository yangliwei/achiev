package org.jeecg.modules.schemecontent.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.schemecontent.entity.SchemeContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 方案内容表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface SchemeContentMapper extends BaseMapper<SchemeContent> {

    List<SchemeContent> queryNotDeletedSchemeContent();

}
