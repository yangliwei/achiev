package org.jeecg.modules.achievscheme.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.achievscheme.entity.AchievScheme;
import org.jeecg.modules.achievscheme.entity.vo.AchievSchemeVo;
import org.jeecg.modules.achievscheme.mapper.AchievSchemeMapper;
import org.jeecg.modules.achievscheme.service.IAchievSchemeService;
import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import org.jeecg.modules.schemeappraiser.service.ISchemeAppraiserService;
import org.jeecg.modules.schemecontent.entity.SchemeContent;
import org.jeecg.modules.schemecontent.entity.vo.SchemeContentVo;
import org.jeecg.modules.schemecontent.service.ISchemeContentService;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;
import org.jeecg.modules.schemedescription.service.ISchemeDescriptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 考评方案总案
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Service
public class AchievSchemeServiceImpl extends ServiceImpl<AchievSchemeMapper, AchievScheme> implements IAchievSchemeService {

    @Autowired
    private ISchemeContentService schemeContentService;
    @Autowired
    private ISchemeDescriptionService schemeDescriptionService;
    @Autowired
    private ISchemeAppraiserService schemeAppraiserService;

    @Override
    public AchievSchemeVo querySchemeById(String id) {
        AchievSchemeVo achievSchemeVo = new AchievSchemeVo();
        AchievScheme achievScheme = this.getById(id);
        BeanUtils.copyProperties(achievScheme, achievSchemeVo);
        // 封装页面展示所需的第二层的数据
        achievSchemeVo.setSchemeContentList(schemeContentService.querySchemeContentVoBySchemeId(id));
        return achievSchemeVo;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrUpdateScheme(AchievSchemeVo achievSchemeVo) {
        AchievScheme achievScheme = new AchievScheme();
        BeanUtils.copyProperties(achievSchemeVo, achievScheme);
        this.saveOrUpdate(achievScheme);
        String schemeId = achievScheme.getId();

        // 得到考评内容的数据
        List<SchemeContentVo> schemeContentVoList = achievSchemeVo.getSchemeContentList();
        List<SchemeContent> schemeContentList = new ArrayList<>();
        List<SchemeDescription> schemeDescriptionList = new ArrayList<>();
        List<SchemeAppraiser> schemeAppraiserList = new ArrayList<>();
        // 遍历得到传入参数内不同层数对应的数据
        for (int i = 0; i < schemeContentVoList.size(); i++) {
            SchemeContentVo schemeContentVo = schemeContentVoList.get(i);
            SchemeContent schemeContent = new SchemeContent();
            BeanUtils.copyProperties(schemeContentVo, schemeContent);
            schemeContent.setSchemeId(schemeId);
            schemeContent.setSort(i + 1);
            schemeContentList.add(schemeContent);

            if (ObjectUtils.isNotEmpty(schemeContentVo.getSchemeDescriptionList())) {
                for (SchemeDescription schemeDescription : schemeContentVo.getSchemeDescriptionList()) {
                    schemeDescription.setSchemeId(schemeId);
                    schemeDescription.setSchemeContentId(i + "");
                }
            }else {
                schemeContentVo.setSchemeDescriptionList(new ArrayList<>());
            }
            if (ObjectUtils.isNotEmpty(schemeContentVo.getSchemeAppraiserList())) {
                for (SchemeAppraiser schemeAppraiser : schemeContentVo.getSchemeAppraiserList()) {
                    schemeAppraiser.setSchemeId(schemeId);
                    schemeAppraiser.setSchemeContentId(i + "");
                }
            } else {
                schemeContentVo.setSchemeAppraiserList(new ArrayList<>());
            }
            schemeDescriptionList.addAll(schemeContentVo.getSchemeDescriptionList());
            schemeAppraiserList.addAll(schemeContentVo.getSchemeAppraiserList());
        }
        // 处理完数据, 先删除原数据再进行添加
        schemeContentService.remove(new LambdaQueryWrapper<SchemeContent>().eq(SchemeContent::getSchemeId, schemeId));
        schemeContentService.saveBatch(schemeContentList);

        Map<String, String> contentIdMap = new HashMap<>();
        for (int i = 0; i < schemeContentList.size(); i++) {
            String id = schemeContentList.get(i).getId();
            contentIdMap.put(i + "", id);
        }
        for (SchemeDescription schemeDescription : schemeDescriptionList) {
            schemeDescription.setSchemeContentId(contentIdMap.get(schemeDescription.getSchemeContentId()));
        }
        schemeDescriptionService.remove(new LambdaQueryWrapper<SchemeDescription>().eq(SchemeDescription::getSchemeId, schemeId));
        schemeDescriptionService.saveBatch(schemeDescriptionList);
        for (SchemeAppraiser schemeAppraiser : schemeAppraiserList) {
            schemeAppraiser.setSchemeContentId(contentIdMap.get(schemeAppraiser.getSchemeContentId()));
        }
        schemeAppraiserService.remove(new LambdaQueryWrapper<SchemeAppraiser>().eq(SchemeAppraiser::getSchemeId, schemeId));
        schemeAppraiserService.saveBatch(schemeAppraiserList);
    }

    @Override
    public List<AchievScheme> listByAppraiser(String id) {
        return this.list(new LambdaQueryWrapper<AchievScheme>().eq(AchievScheme::getCompletedAppraiser, id));
    }

}
