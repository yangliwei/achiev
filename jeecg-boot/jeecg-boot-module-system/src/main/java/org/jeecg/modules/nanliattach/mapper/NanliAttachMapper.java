package org.jeecg.modules.nanliattach.mapper;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.nanliattach.entity.NanliAttach;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 附件表
 * @Author: risk-manage
 * @Date:   2021-10-27
 * @Version: V1.0
 */
public interface NanliAttachMapper extends BaseMapper<NanliAttach> {

    public String getUrlById(@Param("id") String id);

    public List<JSONObject> getUrlList(@Param("ids")String ids);

    List<String> getByUrlList(String[] urls);
}
