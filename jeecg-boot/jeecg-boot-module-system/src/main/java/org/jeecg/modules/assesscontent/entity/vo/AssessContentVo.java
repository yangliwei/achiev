package org.jeecg.modules.assesscontent.entity.vo;

import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jeecg.modules.assesscontent.entity.AssessContent;
import org.jeecg.modules.assessdescription.entity.AssessDescription;
import org.jeecg.modules.assessdescription.entity.vo.AssessDescriptionVo;
import org.jeecg.modules.assessscore.entity.AssessScore;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class AssessContentVo extends AssessContent {

    private String schemeContent;

    private Integer sort;

    private List<AssessDescriptionVo> assessDescriptionList;

    private List<AssessScore> assessScoreList;

}
