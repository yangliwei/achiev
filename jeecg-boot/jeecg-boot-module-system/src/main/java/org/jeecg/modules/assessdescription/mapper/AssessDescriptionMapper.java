package org.jeecg.modules.assessdescription.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.assessdescription.entity.AssessDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.assessdescription.entity.vo.AssessDescriptionVo;

/**
 * @Description: 考评描述表
 * @Author: jeecg-boot
 * @Date:   2023-02-14
 * @Version: V1.0
 */
public interface AssessDescriptionMapper extends BaseMapper<AssessDescription> {

    List<AssessDescriptionVo> queryVoBySchemeId(String schemeId);

    List<AssessDescriptionVo> queryData(String schemeId);

}
