package org.jeecg.modules.assessdescription.service;

import org.jeecg.modules.assessdescription.entity.AssessDescription;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.assessdescription.entity.vo.AssessDescriptionVo;
import org.jeecg.modules.assessscore.entity.AssessScore;

import java.util.List;
import java.util.Map;

/**
 * @Description: 考评描述表
 * @Author: jeecg-boot
 * @Date:   2023-02-14
 * @Version: V1.0
 */
public interface IAssessDescriptionService extends IService<AssessDescription> {

    /**
     * 生成考评程度描述数据
     * @param schemeDescriptionId
     * @param assessContentId
     * @param achievAssessId
     */
    void generateAssessDescription(String schemeDescriptionId, String assessContentId, String achievAssessId, Map<String, List<AssessDescription>> assessDescriptionMap);

    /**
     * 查询页面展示所需要的程度描述
     * @param schemeId
     * @return
     */
    List<AssessDescriptionVo> queryVoBySchemeId(String schemeId);
}

