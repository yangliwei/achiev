package org.jeecg.modules.utils;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 读取excel表格
 * @Author: JiYiHuang
 * @Date: 2022-07-09
 */
public class ReadExcelUtils {
    /**
     * 读取excel表格, 封装成List集合, map的key是对应列首行, value是对应单元格的值
     * @param request
     * @param rowNum 标题行数, 从0开始
     * @return
     * @throws IOException
     */
    public static List<Map<String, String>> readExcel(HttpServletRequest request, Integer rowNum) throws IOException {
        List<Map<String, String>> list = new ArrayList<>();
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            if (entity.getValue().isEmpty()) {
                throw new IOException("请上传xlsx或xls文件");
            }
            if (StringUtils.isBlank(entity.getValue().getOriginalFilename())) {
                throw new IOException("文件名有误, 请改名后再次上传");
            }
            if (entity.getValue().getOriginalFilename().endsWith(".xlsx")) {
                xlsx(list, entity, rowNum);
            } else if (entity.getValue().getOriginalFilename().endsWith(".xls")) {
                xls(list, entity, rowNum);
            } else {
                throw new IOException("只接收xlsx或xls文件");
            }
        }

        return list;
    }

    private static void xlsx(List<Map<String, String>> list, Map.Entry<String, MultipartFile> entity, Integer rowNum) throws IOException {
        XSSFWorkbook sheets = new XSSFWorkbook(entity.getValue().getInputStream());
        // 获取第一个表单Sheet
        XSSFSheet sheetAt = sheets.getSheetAt(0);
        //默认第三行为标题行，i = 2
        XSSFRow titleRow = sheetAt.getRow(rowNum);
        // 循环获取每一行数据
        for (int i = rowNum + 1; i < sheetAt.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = sheetAt.getRow(i);
            LinkedHashMap<String, String> map = new LinkedHashMap<>();
            // 读取每一格内容
            for (int index = 0; index < row.getPhysicalNumberOfCells(); index++) {
                XSSFCell titleCell = titleRow.getCell(index);
                XSSFCell cell = row.getCell(index);
                if (ObjectUtils.isEmpty(cell)) {
                    continue;
                }
                cell.setCellType(CellType.STRING);
                if (cell.getStringCellValue().equals("")) {
                    continue;
                }
                map.put(titleCell.toString(), cell.toString());
            }
            if (map.isEmpty()) {
                continue;
            }
            list.add(map);
        }
    }

    private static void xls(List<Map<String, String>> list, Map.Entry<String, MultipartFile> entity, Integer rowNum) throws IOException {
        HSSFWorkbook sheets = new HSSFWorkbook(entity.getValue().getInputStream());
        // 获取第一个表单Sheet
        HSSFSheet sheetAt = sheets.getSheetAt(0);
        //默认第三行为标题行，i = 2
        HSSFRow titleRow = sheetAt.getRow(rowNum);
        // 循环获取每一行数据
        for (int i = rowNum + 1; i < sheetAt.getPhysicalNumberOfRows(); i++) {
            HSSFRow row = sheetAt.getRow(i);
            LinkedHashMap<String, String> map = new LinkedHashMap<>();
            // 读取每一格内容
            for (int index = 0; index < row.getPhysicalNumberOfCells(); index++) {
                HSSFCell titleCell = titleRow.getCell(index);
                HSSFCell cell = row.getCell(index);
                if (ObjectUtils.isEmpty(cell)) {
                    continue;
                }
                cell.setCellType(CellType.STRING);
                if (cell.getStringCellValue().equals("")) {
                    continue;
                }
                map.put(titleCell.toString(), cell.toString());
            }
            if (map.isEmpty()) {
                continue;
            }
            list.add(map);
        }
    }
}
