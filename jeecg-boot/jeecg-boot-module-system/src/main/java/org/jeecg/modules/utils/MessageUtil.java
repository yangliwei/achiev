package org.jeecg.modules.utils;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.util.DySmsEnum;
import org.jeecg.common.util.DySmsHelper;

/**
 * @Author: admin
 * @Date: 2022-02-16 11:58
 */
public class MessageUtil {

    /**
     * 发送预约服务短信
     */
    public static boolean sendServerMessage(String phone,JSONObject templateParamJson,String smsmode) throws ClientException {

        /**
         * smsmode 短信模板方式  0 .登录模板、1.注册模板、2.忘记密码模板、3.平安安责险预约服务通知短信、4.人保安责险预约服务通知短信
         */
        boolean b = false;
        if (CommonConstant.SMS_TPL_TYPE_0.equals(smsmode)) {
            //登录模板
            b = DySmsHelper.sendSms(phone, templateParamJson, DySmsEnum.LOGIN_TEMPLATE_CODE);
        } else if(CommonConstant.SMS_TPL_TYPE_2.equals(smsmode)) {
            //忘记密码模板
            b = DySmsHelper.sendSms(phone, templateParamJson, DySmsEnum.FORGET_PASSWORD_TEMPLATE_CODE);
        } else if(CommonConstant.SMS_TPL_TYPE_3.equals(smsmode)){
            //安责险预约服务通知短信
            //平安
            b = DySmsHelper.sendSms(phone, templateParamJson, DySmsEnum.PA_APPONIT_MESSAGE_TEMPLATE_CODE);
        }else if(CommonConstant.SMS_TPL_TYPE_4.equals(smsmode)){
            //安责险预约服务通知短信
            //人保
            b = DySmsHelper.sendSms(phone, templateParamJson, DySmsEnum.RB_APPOINT_MESSAGE_TEMPLATE_CODE);
        }
        return b;

    }
}
