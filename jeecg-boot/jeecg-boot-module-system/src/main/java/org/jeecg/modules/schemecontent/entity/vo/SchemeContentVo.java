package org.jeecg.modules.schemecontent.entity.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import org.jeecg.modules.schemecontent.entity.SchemeContent;
import org.jeecg.modules.schemedescription.entity.SchemeDescription;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class SchemeContentVo extends SchemeContent {

    List<SchemeDescription> schemeDescriptionList;

    List<SchemeAppraiser> schemeAppraiserList;

}
