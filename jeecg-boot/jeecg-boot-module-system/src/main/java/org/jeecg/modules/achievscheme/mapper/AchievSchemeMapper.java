package org.jeecg.modules.achievscheme.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.achievscheme.entity.AchievScheme;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 考评方案总案
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface AchievSchemeMapper extends BaseMapper<AchievScheme> {

}
