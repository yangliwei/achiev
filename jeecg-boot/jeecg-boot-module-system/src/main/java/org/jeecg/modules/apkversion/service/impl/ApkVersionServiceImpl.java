package org.jeecg.modules.apkversion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.modules.apkversion.entity.ApkVersion;
import org.jeecg.modules.apkversion.mapper.ApkVersionMapper;
import org.jeecg.modules.apkversion.service.IApkVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: app版本记录
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Service
public class ApkVersionServiceImpl extends ServiceImpl<ApkVersionMapper, ApkVersion> implements IApkVersionService {

    @Autowired
    private ApkVersionMapper apkVersionMapper;

    @Override
    public ApkVersion queryNewOne() {
        return apkVersionMapper.selectOne(new QueryWrapper<ApkVersion>().orderByDesc("create_time").last("limit 1"));
    }
}
