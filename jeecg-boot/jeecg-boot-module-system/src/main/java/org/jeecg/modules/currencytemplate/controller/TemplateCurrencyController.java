package org.jeecg.modules.currencytemplate.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.currencytemplate.entity.TemplateCurrency;
import org.jeecg.modules.currencytemplate.service.ITemplateCurrencyService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 模板通用项
 * @Author: jeecg-boot
 * @Date:   2021-05-27
 * @Version: V1.0
 */
@Api(tags="模板通用项")
@RestController
@RequestMapping("/templatecurrency/templateCurrency")
@Slf4j
public class TemplateCurrencyController extends JeecgController<TemplateCurrency, ITemplateCurrencyService> {
	@Autowired
	private ITemplateCurrencyService templateCurrencyService;
	
	/**
	 * 分页列表查询
	 *
	 * @param templateCurrency
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "模板通用项-分页列表查询")
	@ApiOperation(value="模板通用项-分页列表查询", notes="模板通用项-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TemplateCurrency templateCurrency,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TemplateCurrency> queryWrapper = QueryGenerator.initQueryWrapper(templateCurrency, req.getParameterMap());
		Page<TemplateCurrency> page = new Page<TemplateCurrency>(pageNo, pageSize);
		IPage<TemplateCurrency> pageList = templateCurrencyService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param templateCurrency
	 * @return
	 */
	@AutoLog(value = "模板通用项-添加")
	@ApiOperation(value="模板通用项-添加", notes="模板通用项-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TemplateCurrency templateCurrency) {
		templateCurrencyService.save(templateCurrency);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param templateCurrency
	 * @return
	 */
	@AutoLog(value = "模板通用项-编辑")
	@ApiOperation(value="模板通用项-编辑", notes="模板通用项-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TemplateCurrency templateCurrency) {
		templateCurrencyService.updateById(templateCurrency);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "模板通用项-通过id删除")
	@ApiOperation(value="模板通用项-通过id删除", notes="模板通用项-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		templateCurrencyService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "模板通用项-批量删除")
	@ApiOperation(value="模板通用项-批量删除", notes="模板通用项-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.templateCurrencyService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "模板通用项-通过id查询")
	@ApiOperation(value="模板通用项-通过id查询", notes="模板通用项-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TemplateCurrency templateCurrency = templateCurrencyService.getById(id);
		if(templateCurrency==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(templateCurrency);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param templateCurrency
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TemplateCurrency templateCurrency) {
        return super.exportXls(request, templateCurrency, TemplateCurrency.class, "模板通用项");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return templateCurrencyService.importCurrency(request,response,TemplateCurrency.class);
    }

}
