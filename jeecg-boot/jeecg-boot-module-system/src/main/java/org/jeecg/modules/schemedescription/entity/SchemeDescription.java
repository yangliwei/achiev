package org.jeecg.modules.schemedescription.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 程度描述表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Data
@TableName("scheme_description")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="scheme_description对象", description="程度描述表")
public class SchemeDescription implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**考核内容id*/
	@Excel(name = "考核内容id", width = 15)
    @ApiModelProperty(value = "考核内容id")
    private java.lang.String schemeContentId;
    /**方案id*/
    @Excel(name = "方案id", width = 15)
    @ApiModelProperty(value = "方案id")
    private java.lang.String schemeId;
	/**评分*/
	@Excel(name = "评分", width = 15)
    @ApiModelProperty(value = "评分")
    private java.lang.String descriptionScore;
	/**程度描述*/
	@Excel(name = "程度描述", width = 15)
    @ApiModelProperty(value = "程度描述")
    private java.lang.String descriptionContent;
    /**任务数量描述*/
    @Excel(name = "任务数量描述", width = 15)
    @ApiModelProperty(value = "任务数量描述")
    private java.lang.String shouldCompletedContent;
    /**完成数量描述*/
    @Excel(name = "完成数量描述", width = 15)
    @ApiModelProperty(value = "完成数量描述")
    private java.lang.String actualCompletedContent;
}
