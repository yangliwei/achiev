package org.jeecg.modules.achievassess.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.achievassess.entity.AchievAssess;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessBo;
import org.jeecg.modules.achievassess.entity.vo.AchievAssessVo;

import java.text.ParseException;
import java.util.List;

/**
 * @Description: 绩效考评总表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
public interface IAchievAssessService extends IService<AchievAssess> {

    /**
     * 生成考评表数据
     */
    void generatePerformanceAppraisal();

    /**
     * 查询考评表信息_根据id
     * @param id
     * @return
     */
    AchievAssessVo queryAssessById(String id);

    /**
     * 查询考评表信息_根据用户查该考评表内用户负责的考评数据
     * @param id
     * @param userId
     * @return
     */
    AchievAssessVo queryAssessByIdAndUserId(String id, String userId);

    /**
     * 分页列表查询_查询当前用户所负责的评分的数据
     *
     * @param page
     * @param userId
     * @param date
     * @return
     */
    IPage<AchievAssessVo> queryResponsibleData(Page<AchievAssessVo> page, String userId, String userName, String schemeName, String date);

    /**
     * 查询指定月份的全部数据
     * @param page
     * @param userId
     * @param date
     * @param userName
     * @param schemeName
     * @return
     */
    IPage<AchievAssessVo> querySchemeByAppraisee(Page<AchievAssessVo> page, String userId, String date, String userName, String schemeName);

    /**
     * 查询完成率评分数据
     * @param page
     * @param userId
     * @param date
     * @param name
     * @return
     */
    IPage<AchievAssessVo> queryCompletionRate(Page<AchievAssessVo> page, String userId, String date, String name);

    /**
     * 查询完成率评分数据详细信息
     * @param id
     * @return
     */
    AchievAssessVo queryCompletionRateInfo(String id);

    /**
     * 修改评分人的评分
     * @param achievAssessVo
     */
    void updateAssessScore(AchievAssessVo achievAssessVo);

    /**
     * 修改考评总分的评分
     * @param id
     */
    void updateAssessTotal(String id);

    /**
     * 修改考评方案的平均分
     * @param id
     */
    void updateAssessContentAvg(String id);
    void updateBatchAssessContentAvg(List<String> list);

    /**
     * 修改考评的完成率
     * @param achievAssess
     */
    void updateCompletionRate(AchievAssess achievAssess);

    /**
     * 根据月份导出数据
     * @param date
     * @return
     */
    List<AchievAssessBo> getAssessAppraiseeNameAndScoringCount(String date);


    /**
     * 是否结束考评
     * @return
     */
    boolean isFinishAssess();

    /**
     * 查询最近一次数据的月份
     * @return
     */
    String queryLastDataMonth();

}
