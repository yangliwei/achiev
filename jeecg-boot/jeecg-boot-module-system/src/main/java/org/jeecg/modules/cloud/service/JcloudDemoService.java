package org.jeecg.modules.cloud.service;

import org.jeecg.common.api.vo.Result;

public interface JcloudDemoService {
    Result<String> getMessage(String name);
}
