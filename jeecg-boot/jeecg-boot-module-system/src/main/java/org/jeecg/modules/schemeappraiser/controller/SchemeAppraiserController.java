package org.jeecg.modules.schemeappraiser.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.schemeappraiser.entity.SchemeAppraiser;
import org.jeecg.modules.schemeappraiser.service.ISchemeAppraiserService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 方案考评人表
 * @Author: jeecg-boot
 * @Date:   2023-01-16
 * @Version: V1.0
 */
@Api(tags="方案考评人表")
@RestController
@RequestMapping("/schemeappraiser/schemeAppraiser")
@Slf4j
public class SchemeAppraiserController extends JeecgController<SchemeAppraiser, ISchemeAppraiserService> {
	@Autowired
	private ISchemeAppraiserService schemeAppraiserService;
	
	/**
	 * 分页列表查询
	 *
	 * @param schemeAppraiser
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "方案考评人表-分页列表查询")
	@ApiOperation(value="方案考评人表-分页列表查询", notes="方案考评人表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SchemeAppraiser schemeAppraiser,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SchemeAppraiser> queryWrapper = QueryGenerator.initQueryWrapper(schemeAppraiser, req.getParameterMap());
		Page<SchemeAppraiser> page = new Page<SchemeAppraiser>(pageNo, pageSize);
		IPage<SchemeAppraiser> pageList = schemeAppraiserService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param schemeAppraiser
	 * @return
	 */
	@AutoLog(value = "方案考评人表-添加")
	@ApiOperation(value="方案考评人表-添加", notes="方案考评人表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SchemeAppraiser schemeAppraiser) {
		schemeAppraiserService.save(schemeAppraiser);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param schemeAppraiser
	 * @return
	 */
	@AutoLog(value = "方案考评人表-编辑")
	@ApiOperation(value="方案考评人表-编辑", notes="方案考评人表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SchemeAppraiser schemeAppraiser) {
		schemeAppraiserService.updateById(schemeAppraiser);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "方案考评人表-通过id删除")
	@ApiOperation(value="方案考评人表-通过id删除", notes="方案考评人表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		schemeAppraiserService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "方案考评人表-批量删除")
	@ApiOperation(value="方案考评人表-批量删除", notes="方案考评人表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.schemeAppraiserService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "方案考评人表-通过id查询")
	@ApiOperation(value="方案考评人表-通过id查询", notes="方案考评人表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SchemeAppraiser schemeAppraiser = schemeAppraiserService.getById(id);
		if(schemeAppraiser==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(schemeAppraiser);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param schemeAppraiser
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SchemeAppraiser schemeAppraiser) {
        return super.exportXls(request, schemeAppraiser, SchemeAppraiser.class, "方案考评人表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, SchemeAppraiser.class);
    }

}
