/**
 * 监听企业端和用户端获取eid方式  --- 监管端获取eid方式
 * temp_param 父组件传过来的值
 * temp_param有值就直接应用，否则从本地拿数据
 */
export const HseentMixin = {
  data(){
    return {
      queryParam:{},
    }
  },
  watch:{
    //监听路由变化
    $route:{
      immediate:true,
      handler(to, from){
        //需要监听的参数 this.$route.query.eid路径上如果没有值从本地获取对应企业eid
        this.queryParam.guaranteeSlip = this.$route.query.guaranteeSlip
        this.queryParam.entName = this.$route.query.entName
        this.queryParam.industry = this.$route.query.industry
        console.log('this.queryParam.industry')
        console.log(this.queryParam.industry)

        if (typeof this.loadData === 'function' && this.queryParam.guaranteeSlip) {
          this.loadData()
        }else {
          return
        }
      }
    }
  },

}